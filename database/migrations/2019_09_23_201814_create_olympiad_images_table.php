<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOlympiadImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olympiad_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('images')->nullable();
            $table->integer('olympiad_id')->unsigned();
            $table->foreign('olympiad_id')->references('id')->on('olympiads');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olympiad_images');
    }
}
