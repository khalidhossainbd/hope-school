<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMenusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mainmenu')->nullable();
            $table->string('submenu')->nullable();
            $table->string('url_name')->nullable();
            $table->string('images')->nullable();
            $table->text('content')->nullable();
            $table->integer('layout')->default(1);
            $table->integer('status')->default(1);
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menuses');
    }
}
