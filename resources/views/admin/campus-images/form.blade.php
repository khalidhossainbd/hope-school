<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ isset($campusimage->title) ? $campusimage->title : ''}}" >
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('images') ? 'has-error' : ''}}">
    <label for="images" class="control-label">{{ 'Images' }}</label>
    <input class="form-control" name="images" type="text" id="images" value="{{ isset($campusimage->images) ? $campusimage->images : ''}}" >
    {!! $errors->first('images', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('campuslife_id') ? 'has-error' : ''}}">
    <label for="campuslife_id" class="control-label">{{ 'Campuslife Id' }}</label>
    <input class="form-control" name="campuslife_id" type="number" id="campuslife_id" value="{{ isset($campusimage->campuslife_id) ? $campusimage->campuslife_id : ''}}" >
    {!! $errors->first('campuslife_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
