@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">FacilityImage {{ $facilityimage->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/kadmin/facility-images') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/kadmin/facility-images/' . $facilityimage->id . '/edit') }}" title="Edit FacilityImage"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('kadmin/facilityimages' . '/' . $facilityimage->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete FacilityImage" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $facilityimage->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $facilityimage->title }} </td></tr><tr><th> Images </th><td> {{ $facilityimage->images }} </td></tr><tr><th> Facilities Id </th><td> {{ $facilityimage->facilities_id }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
