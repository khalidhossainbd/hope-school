@extends('layouts.dashboardlayout')

@section('content')

<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="panel-title">Create New Branch</h3>
                            <a href="{{ url('/kadmin/branches') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        </div>
                        <div class="col-md-6"> 
                            @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="panel-body">
                    <div class="row">
                        <form method="POST" action="{{ url('/kadmin/branches') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Branch Name</label>
                                <input type="text" class="form-control" id="title" placeholder="Branch Name" name="title" required="required">
                            </div>
                            <div class="form-group">
                                <label for="title">Branch Url name <small> ( No space, No special character allow. Only '-' can use )</small> </label>
                                <input type="text" class="form-control" id="url_name" placeholder="Branch Url Name" name="url_name" required="required">
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" id="address" placeholder="Branch School Addres" name="address" required="required">
                            </div>
                            <div class="form-group">
                                <label for="content">Content</label>
                                <textarea id="editor" class="form-control" rows="5" name="content"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Status</label>
                                <select class="form-control" id="status" name="status" required="required">
                                    <option>Select Option</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="facebook">Facebook Url</label>
                                <input type="text" class="form-control" id="facebook" placeholder="Facebook Page Link" name="facebook">
                            </div>
                            <div class="form-group">
                                <label for="youtube">Youtube Url</label>
                                <input type="text" class="form-control" id="youtube" placeholder="Youtube Page Link" name="youtube">
                            </div>
                            <div class="form-group">
                                <label for="twitter">Twitter Url</label>
                                <input type="text" class="form-control" id="twitter" placeholder="Twitter Page Link" name="twitter">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Save">
                            </div>
                            
                        </div>
                       {{--  <div class="col-md-6"></div> --}}
                        </form>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
    
@endsection
