@extends('layouts.dashboardlayout')

@section('content')

<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="panel-title">Branch Details</h3>
                            <a href="{{ url('/kadmin/branches') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <a href="{{ url('/kadmin/branches/' . $branch->id . '/edit') }}" title="Edit Branch"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                            
                        </div>
                        <div class="col-md-6">
                            
                        </div>
                    </div>
                    
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="20%">Branch Name</th>
                                                <td>
                                                    {{ $branch->title }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th width="20%">Url Name</th>
                                                <td>
                                                    {{ $branch->url_name }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Address</th>
                                                <td>{{ $branch->address }}</td>
                                            </tr>
                                            
                                            <tr>
                                                <th>Status</th>
                                                <td>
                                                    @php 
                                                        if($branch->status >0){
                                                            echo "Publish";
                                                        }else{
                                                            echo"Unpublish";
                                                        }

                                                    @endphp
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Facebook Url</th>
                                                <td>{{ $branch->facebook }}</td>
                                            </tr>
                                            <tr>
                                                <th>Youtube Url</th>
                                                <td>{{ $branch->youtube }}</td>
                                            </tr>
                                            <tr>
                                                <th>Twitter Url</th>
                                                <td>{{ $branch->twitter }}</td>
                                            </tr>
                                            
                                            <tr>
                                                <th>Content</th>
                                                <td>
                                                    {!! $branch->content !!}
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
    {{-- <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Branch {{ $branch->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/kadmin/branches') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/kadmin/branches/' . $branch->id . '/edit') }}" title="Edit Branch"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('kadmin/branches' . '/' . $branch->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Branch" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $branch->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $branch->title }} </td></tr><tr><th> Address </th><td> {{ $branch->address }} </td></tr><tr><th> Content </th><td> {{ $branch->content }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
