@extends('layouts.dashboardlayout')

@section('content')

<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="panel-title">Edit Branch Information</h3>
                            <a href="{{ url('/kadmin/branches') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        </div>
                        <div class="col-md-6"> 
                            @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="panel-body">
                    <div class="row">
                        <form method="POST" action="{{ url('/kadmin/branches/' . $branch->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Branch Name</label>
                                <input type="text" class="form-control" id="title" value="{{ $branch->title }}" name="title" required="required">
                            </div>
                            <div class="form-group">
                                <label for="title">Branch Url name <small> ( No space, No special character allow. Only '-' can use )</small> </label>
                                <input type="text" class="form-control" id="url_name" value="{{ $branch->url_name }}" name="url_name" required="required">
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" id="address" value="{{ $branch->address }}" name="address" required="required">
                            </div>
                            <div class="form-group">
                                <label for="content">Content</label>
                                <textarea id="editor" class="form-control" rows="5" name="content">{!! $branch->content !!}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Status</label>
                                <select class="form-control" id="status" name="status" required="required">
                                    @php
                                    if($branch->status == 1){
                                    @endphp
                                        <option selected="selected" value="1">Publish</option>
                                        <option value="0">Unpublish</option>
                                    @php
                                    }else{
                                    @endphp
                                        <option value="1">Publish</option>
                                        <option selected="selected" value="0">Unpublish</option>
                                    @php
                                    }
                                    @endphp
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="facebook">Facebook Url</label>
                                <input type="text" class="form-control" id="facebook" value="{{ $branch->facebook }}" name="facebook">
                            </div>
                            <div class="form-group">
                                <label for="youtube">Youtube Url</label>
                                <input type="text" class="form-control" id="youtube" value="{{ $branch->youtube }}" name="youtube">
                            </div>
                            <div class="form-group">
                                <label for="twitter">Twitter Url</label>
                                <input type="text" class="form-control" id="twitter" value="{{ $branch->twitter }}" name="twitter">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Update">
                            </div>
                            
                        </div>
                       {{--  <div class="col-md-6"></div> --}}
                        </form>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
    {{-- <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Edit Branch #{{ $branch->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/kadmin/branches') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/kadmin/branches/' . $branch->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('admin.branches.form', ['formMode' => 'edit'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
