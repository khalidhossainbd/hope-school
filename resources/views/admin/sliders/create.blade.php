@extends('layouts.dashboardlayout')

@section('content')

<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="panel-title">Add New Slider Images </h3>
                            <a href="{{ url('/kadmin/sliders') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        </div>
                        <div class="col-md-6"> 
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="panel-body">
                    <div class="row">
                        <form method="POST" action="{{ url('/kadmin/sliders') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Image Title</label>
                                <input type="text" class="form-control" id="title" placeholder="Image Title" name="title" required="required">
                            </div>
                            <div class="form-group">
                                <label for="date">Date (For Serial)</label>
                                <input type="date" class="form-control" id="title" placeholder="Date For image Serial" name="date" required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Status</label>
                                <select class="form-control" id="status" name="status" required="required">
                                    <option>Select Option</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label for="files">File input</label>
                                <input type="file" id="files" name="images">
                                <p class="help-block">Image size must be 1920 x 700 PX</p>
                            </div>
                            
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Save">
                            </div>
                        </div>
                        <div class="col-md-6" style="padding: 0 50px;">
                            <div class="form-group">
                                <label>Selected Image</label>
                                <img style="max-height: 300px;" class="img-responsive" id="image" />
                            </div>
                        </div>
                       
                        </form>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>

    {{-- <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Create New Slider</div>
                    <div class="card-body">
                        <a href="{{ url('/kadmin/sliders') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/kadmin/sliders') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('admin.sliders.form', ['formMode' => 'create'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
