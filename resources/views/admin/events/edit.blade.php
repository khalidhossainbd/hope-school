@extends('layouts.dashboardlayout')

@section('content')

<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="panel-title">Edit School Event</h3>
                            <a href="{{ url('/kadmin/events') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        </div>
                        <div class="col-md-6"> 
                            @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="panel-body">
                    <div class="row">
                        <form method="POST" action="{{ url('/kadmin/events/' . $event->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}
                        <div class="col-md-6">
                            

                            <div class="form-group">
                                <label for="title">Event Title</label>
                                <input type="text" class="form-control" id="title" value="{{ $event->title }}" name="title" required="required">
                            </div>
                            <div class="form-group">
                                <label for="subtitle">Sub title</label>
                                <textarea class="form-control" rows="5" name="subtitle">
                                    {{ $event->subtitle }}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="subtitle">Event Date</label>
                                <input type="date" class="form-control" id="title" value="{{ $event->date }}" name="date" required="required">
                            </div>
                            
                            <div class="form-group">
                                <label for="exampleInputEmail1">Status</label>
                                <select class="form-control" id="status" name="status" required="required">
                                    @php
                                    if($event->status == 1){
                                    @endphp
                                        <option selected="selected" value="1">Publish</option>
                                        <option value="0">Unpublish</option>
                                    @php
                                    }else{
                                    @endphp
                                        <option value="1">Publish</option>
                                        <option selected="selected" value="0">Unpublish</option>
                                    @php
                                    }
                                    @endphp
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="content">Content</label>
                                <textarea class="form-control tinymce" rows="5" name="content">
                                    {!! $event->content !!}
                                </textarea>
                            </div>
                            
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Update">
                            </div>
                            
                        </div>
                        <div class="col-md-6" style="padding: 0 50px;">
                            <div class="form-group">
                                <label for="files">File input Icon</label>
                                <input type="file" id="files" name="image">
                                <p class="help-block">Image size must be 512 x 512 PX</p>
                                <p class="help-block">Image size must be less then 2 MB</p>
                            </div>
                            <div class="form-group">
                                <label>Selected Image</label>
                                <img style="max-height: 300px;" class="img-responsive" id="image" src="{{ asset('uploads/events/'.$event->image) }}" />
                            </div>
                        </div>
                        
                            
                        </form>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
    {{-- <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Edit Event #{{ $event->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/kadmin/events') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/kadmin/events/' . $event->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('admin.events.form', ['formMode' => 'edit'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
