@extends('layouts.dashboardlayout')

@section('content')

<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="panel-title">School Event Details</h3>
                            <a href="{{ url('/kadmin/events') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <a href="{{ url('/kadmin/events/' . $event->id . '/edit') }}" title="Edit Event"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                            {{-- <form method="POST" action="{{ url('kadmin/events' . '/' . $event->id) }}" accept-charset="UTF-8" style="display:inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Event" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                            </form> --}}
                        </div>
                        <div class="col-md-6">
                            
                        </div>
                    </div>
                    
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="20%">Title</th>
                                                <td>
                                                    {{ $event->title }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Sub Menu</th>
                                                <td>{{ $event->subtitle }}</td>
                                            </tr>
                                            
                                            <tr>
                                                <th>Status</th>
                                                <td>
                                                    @php 
                                                        if($event->status >0){
                                                            echo "Publish";
                                                        }else{
                                                            echo"Unpublish";
                                                        }

                                                    @endphp
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Image</th>
                                                <td>
                                                    <img src="{{ asset('uploads/events/'.$event->image) }}" alt="image" style="max-height: 150px;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Content</th>
                                                <td>
                                                    {!! $event->content !!}
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
    {{-- <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Event {{ $event->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/kadmin/events') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/kadmin/events/' . $event->id . '/edit') }}" title="Edit Event"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('kadmin/events' . '/' . $event->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Event" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $event->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $event->title }} </td></tr><tr><th> Subtitle </th><td> {{ $event->subtitle }} </td></tr><tr><th> Date </th><td> {{ $event->date }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
