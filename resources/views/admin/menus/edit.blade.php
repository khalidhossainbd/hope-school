@extends('layouts.dashboardlayout')

@section('content')

<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="panel-title">Edit Menu</h3>
                            <a href="{{ url('/kadmin/menus') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        </div>
                        <div class="col-md-6"> 
                            @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="panel-body">
                    <div class="row">
                        <form method="POST" action="{{ url('/kadmin/menus/' . $menu->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Main Menu</label>
                                <select class="form-control" id="status" name="mainmenu" required="required">

                                    <option>Select Option</option>
                                    <option {{ $menu->mainmenu == '1' ? 'selected' : '' }} value="1">Menu-1 (About IHSB)</option>
                                    <option {{ $menu->mainmenu == '2' ? 'selected' : '' }} value="2">Menu-2 (Achievements)</option>
                                    <option {{ $menu->mainmenu == '3' ? 'selected' : '' }} value="3">Menu-3 (Academics)</option>
                                    <option {{ $menu->mainmenu == '4' ? 'selected' : '' }} value="4">Menu-4 (Admission)</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="title">Sub Menu Name</label>
                                <input type="text" class="form-control" id="title" value="{{ $menu->submenu }}" name="submenu" required="required">
                            </div>
                            <div class="form-group">
                                <label for="title">Menu Url <small>( No space, No special character allow. Only '-' can use )</small> </label>
                                <input type="text" class="form-control" id="url_name" value="{{ $menu->url_name }}" name="url_name" required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Layout</label>
                                <select class="form-control" id="status" name="layout" required="required">
                                    <option {{ $menu->layout == '1' ? 'selected' : '' }} value="1">Layout-1</option>
                                    <option {{ $menu->layout == '2' ? 'selected' : '' }} value="2">Layout-2</option>
                                    <option {{ $menu->layout == '3' ? 'selected' : '' }} value="3">Layout-3</option>
                                    <option {{ $menu->layout == '4' ? 'selected' : '' }} value="4">Layout-4</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Status</label>
                                <select class="form-control" id="status" name="status" required="required">
                                    <option {{ $menu->status == '1' ? 'selected' : '' }} value="1">Publish</option>
                                    <option {{ $menu->status == '0' ? 'selected' : '' }} value="0">Unpublish</option>         
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="content">Content</label>
                                <textarea id="editor" class="form-control" rows="5" name="content">{{ $menu->content }}</textarea>
                            </div>
                            
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Update">
                            </div>
                            
                        </div>
                        <div class="col-md-6" style="padding: 0 50px;">
                            <div class="form-group">
                                <label for="files">File input</label>
                                <input type="file" id="files" name="images">
                                <p class="help-block">Image size must be 600 x 400 PX</p>
                                <p class="help-block">Image size must be less then 2 MB</p>
                            </div>
                            <div class="form-group">
                                <label>Selected Image</label>
                                <img class="img-responsive" id="image" src="{{ asset('uploads/pages/'.$menu->images) }}" style="max-height: 300px;" />
                            </div>
                        </div>
                        
                            
                        </form>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
    {{-- <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Edit Menu #{{ $menu->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/kadmin/menus') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/kadmin/menus/' . $menu->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('admin.menus.form', ['formMode' => 'edit'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
