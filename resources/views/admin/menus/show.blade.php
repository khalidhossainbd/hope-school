@extends('layouts.dashboardlayout')

@section('content')

<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="panel-title">Menu Details</h3>
                            <a href="{{ url('/kadmin/menus') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <a href="{{ url('/kadmin/menus/' . $menu->id . '/edit') }}" title="Edit Menu"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                            {{-- <form method="POST" action="{{ url('kadmin/menus' . '/' . $menu->id) }}" accept-charset="UTF-8" style="display:inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Menu" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                            </form> --}}
                        </div>
                        <div class="col-md-6">
                            
                        </div>
                    </div>
                    
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th>Main Menu</th>
                                                <td>
                                                    @php 
                                                        if($menu->mainmenu ==1){
                                                            echo "Menu-1 (About IHSB)";
                                                        }elseif($menu->mainmenu ==2){
                                                            echo"Menu-2 (Achievements)";
                                                        }elseif($menu->mainmenu ==3){
                                                            echo"Menu-3 (Academics)";
                                                        }elseif($menu->mainmenu ==4){
                                                            echo("Menu-4 (Admission)");
                                                        }

                                                    @endphp
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Sub Menu</th>
                                                <td>{{ $menu->submenu }}</td>
                                            </tr>
                                            <tr>
                                                <th>Menu Url Name</th>
                                                <td>{{ $menu->url_name }}</td>
                                            </tr>
                                            <tr>
                                                <th>Layout</th>
                                                <td>
                                                    @php 
                                                        if($menu->layout == 1){
                                                            echo "Layout-1 ";
                                                        }elseif($menu->layout == 2){
                                                            echo"Layout-2";
                                                        }else{
                                                            echo"Layout-3";   
                                                        }
                                                    @endphp
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                <td>
                                                    @php 
                                                        if($menu->status >0){
                                                            echo "Publish";
                                                        }else{
                                                            echo"Unpublish";
                                                        }

                                                    @endphp
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Images</th>
                                                <td>
                                    <img src="{{ asset('uploads/pages/'.$menu->images) }}" alt="image" style="max-height: 300px;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Content</th>
                                                <td>
                                                    {!! $menu->content !!}
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
    {{-- <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Menu {{ $menu->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/kadmin/menus') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/kadmin/menus/' . $menu->id . '/edit') }}" title="Edit Menu"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('kadmin/menus' . '/' . $menu->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Menu" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $menu->id }}</td>
                                    </tr>
                                    <tr><th> Mainmenu </th><td> {{ $menu->mainmenu }} </td></tr><tr><th> Submenu </th><td> {{ $menu->submenu }} </td></tr><tr><th> Images </th><td> {{ $menu->images }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
