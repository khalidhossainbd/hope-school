<div class="form-group {{ $errors->has('mainmenu') ? 'has-error' : ''}}">
    <label for="mainmenu" class="control-label">{{ 'Mainmenu' }}</label>
    <input class="form-control" name="mainmenu" type="text" id="mainmenu" value="{{ isset($menu->mainmenu) ? $menu->mainmenu : ''}}" >
    {!! $errors->first('mainmenu', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('submenu') ? 'has-error' : ''}}">
    <label for="submenu" class="control-label">{{ 'Submenu' }}</label>
    <input class="form-control" name="submenu" type="text" id="submenu" value="{{ isset($menu->submenu) ? $menu->submenu : ''}}" >
    {!! $errors->first('submenu', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('images') ? 'has-error' : ''}}">
    <label for="images" class="control-label">{{ 'Images' }}</label>
    <input class="form-control" name="images" type="text" id="images" value="{{ isset($menu->images) ? $menu->images : ''}}" >
    {!! $errors->first('images', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
    <label for="content" class="control-label">{{ 'Content' }}</label>
    <textarea class="form-control" rows="5" name="content" type="textarea" id="content" >{{ isset($menu->content) ? $menu->content : ''}}</textarea>
    {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('layout') ? 'has-error' : ''}}">
    <label for="layout" class="control-label">{{ 'Layout' }}</label>
    <input class="form-control" name="layout" type="number" id="layout" value="{{ isset($menu->layout) ? $menu->layout : ''}}" >
    {!! $errors->first('layout', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('stauts') ? 'has-error' : ''}}">
    <label for="stauts" class="control-label">{{ 'Stauts' }}</label>
    <input class="form-control" name="stauts" type="number" id="stauts" value="{{ isset($menu->stauts) ? $menu->stauts : ''}}" >
    {!! $errors->first('stauts', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
