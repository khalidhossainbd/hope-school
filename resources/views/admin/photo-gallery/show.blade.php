@extends('layouts.dashboardlayout')

@section('content')

<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="panel-title">Gallery Image List</h3>
                            <a href="{{ url('/kadmin/photo-gallery') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <a href="{{ url('/kadmin/photo-gallery/' . $photo->id . '/edit') }}" title="Edit Menu"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        </div>
                        <div class="col-md-6">
                            
                        </div>
                    </div>
                    
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            
                                            <tr>
                                                <th>Gallery Title</th>
                                                <td>{{ $photo->title }}</td>
                                            </tr>
                                            <tr>
                                                <th>Event Date</th>
                                                <td>{{ $photo->date }}</td>
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                <td>
                                                    @php 
                                                        if($photo->status >0){
                                                            echo "Publish";
                                                        }else{
                                                            echo"Unpublish";
                                                        }

                                                    @endphp
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Serial Date</th>
                                                <td>{{ $photo->sl_date }}</td>
                                            </tr>
                                            <tr>
                                                <th>Content</th>
                                                <td>
                                                    {!! $photo->content !!}
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($photo->GalleryImage as $item)
                        <div class="col-md-3">
                            <div class="img-thumbnail">
                                <p style="text-align: center;">{{$item->title}}</p>
                                <img src="{{ asset('uploads/photogallery/'.$item->images) }}" alt="image" class="img-responsive">
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
    
@endsection
