@extends('layouts.dashboardlayout')

@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <!-- OVERVIEW -->
                <div class="panel panel-headline">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="panel-title">Branches Activities List</h3>
                                <a href="{{ url('/kadmin/branches-activity/create') }}" class="btn btn-success btn-sm" title="Add New Branch">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                                </a>
                            </div>
                            <div class="col-md-6">
                                {{-- <form method="GET" action="{{ url('/kadmin/branches-activity') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                        <span class="input-group-btn"><button class="btn btn-success btn-sm" type="button">Search</button></span>
                                    </div>
                                </form> --}}
                            </div>
                        </div>
                        
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Sl No.</th>
                                                        <th>Branch Name</th>
                                                        <th>Branch Activity Name</th>
                                                        <th>Sl Date</th>
                                                        <th>Status</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(count($activity)>0)
                                                @foreach($activity as $item)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $item->Branch->title }}</td>
                                                        <td>{{ $item->title }}</td>
                                                        <td>{{ $item->sl_date }}</td>
                                                        <td>
                                                            @php 
                                                                if($item->status >0){
                                                                    echo "Publish";
                                                                }else{
                                                                    echo"Unpublish";
                                                                }

                                                            @endphp
                                                        </td>
                                                        <td>
                                                            {{-- <a href="{{ url('/kadmin/branches-activity/' . $item->id) }}" title="View Branch"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a> --}}
                                                            <a href="{{ url('/kadmin/branches-activity/' . $item->id . '/edit') }}" title="Edit Branch"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                                            <!-- {{-- <form method="POST" action="{{ url('/kadmin/branches' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                                {{ method_field('DELETE') }}
                                                                {{ csrf_field() }}
                                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Branch" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                            </form> --}} -->
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                    @else
                                                    <tr>
                                                        <td colspan="6" class="text-center">No Data Found</td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                           
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END OVERVIEW -->
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection
