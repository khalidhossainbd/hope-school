@extends('layouts.dashboardlayout')

@section('content')

<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="panel-title">Create New Branch Activity</h3>
                            <a href="{{ url('/kadmin/branches-activity') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        </div>
                        <div class="col-md-6"> 
                            @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="panel-body">
                    <div class="row">
                        <form method="POST" action="{{ url('/kadmin/branches-activity/' . $activity->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="branch_id">Branch Name</label>
                                <select class="form-control" id="" name="branch_id" required="required">
                                    
                                    @foreach($branches as $item)
                                        @if( $item->id == $activity->branch_id)
                                        <option selected="selected" value="{{ $item->id }}">{{ $item->title }}
                                        </option>
                                        @endif
                                        <option value="{{ $item->id }}">{{ $item->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="address">Activity Title</label>
                                <input type="text" class="form-control" id="address" value="{{ $activity->title }}" name="title" required="required">
                            </div>
                            <div class="form-group">
                                <label for="content">Content</label>
                                <textarea class="form-control" rows="3" name="content">{{ $activity->content }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="subtitle">Serial Date</label>
                                <input type="date" class="form-control" id="title" value="{{ $activity->sl_date }}" name="sl_date" required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Status</label>
                                <select class="form-control" id="status" name="status" required="required">
                                    @php
                                    if($activity->status == 1){
                                    @endphp
                                        <option selected="selected" value="1">Publish</option>
                                        <option value="0">Unpublish</option>
                                    @php
                                    }else{
                                    @endphp
                                        <option value="1">Publish</option>
                                        <option selected="selected" value="0">Unpublish</option>
                                    @php
                                    }
                                    @endphp
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Update">
                            </div>
                            
                        </div>
                       {{--  <div class="col-md-6"></div> --}}
                        </form>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
    
@endsection
