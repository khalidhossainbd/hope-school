@extends('layouts.dashboardlayout')

@section('content')

<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="panel-title">Add New Images in School Branches</h3>
                            <a href="{{ url('/kadmin/branches-images') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        </div>
                        <div class="col-md-6"> 
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="panel-body">
                    <div class="row">
                        <form method="POST" action="{{ url('/kadmin/branches-images') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="branch_id">Branch Name</label>
                                <select class="form-control demo" id="" name="branch_id" required="required">
                                    <option>Select Option</option>
                                    @foreach($branches as $item)
                                    <option value="{{ $item->id }}">{{ $item->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="branch_id">Branch Activity Title</label>
                                <select class="form-control demo1" id="" name="branchactivity_id" required="required">
                                    
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title">Image Title</label>
                                <input type="text" class="form-control" id="title" placeholder="Image Title" name="title" required="required">
                            </div>
                            
                            
                            <div class="form-group">
                                <label for="files">File input</label>
                                <input type="file" id="files" name="images[]" multiple="multiple">
                                <p class="help-block">Image size must be 400 x 300 PX</p>
                            </div>
                            
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Save">
                            </div>
                        </div>
                        <div class="col-md-6" style="padding: 0 50px;">
                            <div class="form-group">
                                <label>Selected Image</label>
                                <img style="max-height: 300px;" class="img-responsive" id="image" />
                            </div>
                        </div>
                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('javascript')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('change', '.demo', function(){
            // console.log('It is ok');
            var branch_id = $(this).val();
            // console.log(div_id);
            // var $this = $(this);
            var op =" ";

            $.ajax({
                type:'get',
                url:'{{ url('/kadmin/find_activity')}}',
                data:{ 'id' : branch_id},
                success: function(data){
                    // console.log("ok");
                    // console.log(data.length);
                    op+= '<option value="0" selected disabled >--select an option-- </option>';

                    for (var i = 0; i < data.length; i++) {
                        op+= '<option value="'+ data[i].id +'">'+ data[i].title + '</option>';
                        }

                    // $this.closest('.panel-body').find('.demo1').html(op);
                    $('.demo1').html(op);

                },
                error:function(){

                }
            });
            });
        });
    </script>
@endsection