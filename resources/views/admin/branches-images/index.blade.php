@extends('layouts.dashboardlayout')

@section('content')

<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="panel-title">Branches Images List</h3>
                            <a href="{{ url('/kadmin/branches-images/create') }}" class="btn btn-success btn-sm" title="Add New BranchesImage">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        </div>
                        <div class="col-md-6">
                            {{-- <form method="GET" action="{{ url('/kadmin/branches-images') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form> --}}
                        </div>
                    </div>
                    
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Sl No.</th>
                                                    <th>Branch Name</th>
                                                    <th>Activity Title</th>
                                                    <th>Image Title</th>
                                                    <th style="width: 50%">Image</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($branchesimages as $item)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $item->Branchactivity->Branch->title }}</td>
                                                     <td>{{ $item->Branchactivity->title }}</td>
                                                    <td>{{ $item->title }}</td>
                                                    <td>
                                                        <img src="{{ asset('uploads/branches/'.$item->images) }}" alt="image" style="max-height: 100px">
                                                        </td>
                                                    
                                                    <td>
                                                        {{-- <a href="{{ url('/kadmin/branches-images/' . $item->id) }}" title="View BranchesImage"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a> --}}
                                                        {{-- <a href="{{ url('/kadmin/branches-images/' . $item->id . '/edit') }}" title="Edit BranchesImage"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a> --}}

                                                    <form method="POST" action="{{ url('/kadmin/branches-images' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete BranchesImage" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa     fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                    </form> 
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                         <div class="pagination-wrapper"> {!! $branchesimages->appends(['search' => Request::get('search')])->render() !!} </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>


    {{-- <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Branchesimages</div>
                    <div class="card-body">
                        <a href="{{ url('/kadmin/branches-images/create') }}" class="btn btn-success btn-sm" title="Add New BranchesImage">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <form method="GET" action="{{ url('/kadmin/branches-images') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Title</th><th>Images</th><th>Branch Id</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($branchesimages as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->images }}</td>
                                        <td>{{ $item->branch_id }}</td>
                                        <td>
                                            <a href="{{ url('/kadmin/branches-images/' . $item->id) }}" title="View BranchesImage"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/kadmin/branches-images/' . $item->id . '/edit') }}" title="Edit BranchesImage"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/kadmin/branches-images' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete BranchesImage" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $branchesimages->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
