<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ isset($eventsimage->title) ? $eventsimage->title : ''}}" >
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('images') ? 'has-error' : ''}}">
    <label for="images" class="control-label">{{ 'Images' }}</label>
    <input class="form-control" name="images" type="text" id="images" value="{{ isset($eventsimage->images) ? $eventsimage->images : ''}}" >
    {!! $errors->first('images', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('events_id') ? 'has-error' : ''}}">
    <label for="events_id" class="control-label">{{ 'Events Id' }}</label>
    <input class="form-control" name="events_id" type="number" id="events_id" value="{{ isset($eventsimage->events_id) ? $eventsimage->events_id : ''}}" >
    {!! $errors->first('events_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
