
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ihsbd.net | @yield('title')</title>
	{{-- <meta name="robots" content="noindex"> --}}
	<meta name="description" content="International Hope School Bangladesh">
	<meta name="keywords" content="International Hope School Bangladesh ">
	<meta name="author" content="International Hope School Bangladesh">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta property="og:url" content="http://ihsbd.net/" />
	<meta property="og:site_name" content="Hope School Bangladesh" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<link rel="icon" href="{{ asset('dist/images/icon.png') }}" type="image/png">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/lib/font-awesome/css/font-awesome.min.css') }}">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />

	<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

	<link rel="stylesheet" href="{{ asset('dist/css/main.css') }}">

</head>
<body>


@include('partials.newnavber')

@yield('content')

@include('partials.homefooter')  

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
	  <!-- Modal content-->
	  <div class="modal-content" style="position: relative;">
		  <a href="https://119.148.19.67/edutech/Online_Admission.html" target="_blank">
			<img class="img-thumbnail" src="{{ asset('dist/images/admission.jpg') }}" alt="Modal">
		</a>
		<button type="button" class="btn btn-default" data-dismiss="modal" style="position: absolute; top:10px; left:10px;">X</button>
	  </div>
  
	</div>
  </div>
	


	<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
	  <script>
	    AOS.init();
	  </script>

	<script>
		$(document).ready(function(){

		$('#itemslider').carousel({ interval: 3000 });

		$('.carousel-showmanymoveone .item').each(function(){
		var itemToClone = $(this);

		for (var i=1;i<6;i++) {
		itemToClone = itemToClone.next();

		if (!itemToClone.length) {
		itemToClone = $(this).siblings(':first');
		}

		itemToClone.children(':first-child').clone()
		.addClass("cloneditem-"+(i))
		.appendTo($(this));
		}
		});
		});

	</script>

<script type="text/javascript">
	$(window).load(function(){        
		$('#myModal').modal('show');
	});
</script>


@yield('java_script')
	
</body>
</html>