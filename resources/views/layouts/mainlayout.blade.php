<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ihsbd.net | @yield('title')</title>
	{{-- <meta name="robots" content="noindex"> --}}
	<meta name="description" content="International Hope School Bangladesh">
	<meta name="keywords" content="International Hope School Bangladesh ">
	<meta name="author" content="International Hope School Bangladesh">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta property="og:url" content="http://ihsbd.net/" />
	<meta property="og:site_name" content="Hope School Bangladesh" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<link rel="icon" href="{{ asset('dist/images/icon.png') }}" type="image/png">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<link rel="stylesheet" type="text/css" href="{{ asset('dist/lib/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/lib/animate-css/animate.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/lib/font-awesome/css/font-awesome.min.css') }}">
	<script type="text/javascript" src="{{ asset('dist/lib/jquery/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('dist/lib/bootstrap/js/bootstrap.js') }}"></script>


	<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/custom.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/mystyle.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/newcss.css') }}">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />

	<!-- Start WOWSlider.com HEAD section -->
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/plugin/engine1/style.css') }}" />
	<script type="text/javascript" src="{{ asset('dist/plugin/engine1/jquery.js') }}"></script>
	<!-- End WOWSlider.com HEAD section -->


	

</head>
<body>

	@include('partials.mainnavber')

 	@yield('content')

 	@include('partials.homefooter')  



	<script src='https://cdnjs.cloudflare.com/ajax/libs/react/15.6.1/react.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/react/15.6.1/react-dom.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/classnames/2.2.5/index.min.js'></script>

	{{-- <script type="text/javascript" src="dist/js/sliderjs.js"></script> --}}
	<script type="text/javascript">
		$('.dropdown-toggle').click(function(e) {
		  if ($(document).width() > 768) {
		    e.preventDefault();

		    var url = $(this).attr('href');

		       
		    if (url !== '#') {
		    
		      window.location.href = url;
		    }

		  }
		});
	</script>

	<script type="text/javascript">
		$('.carousel[data-type="multi"] .item').each(function(){
		  var next = $(this).next();
		  if (!next.length) {
		    next = $(this).siblings(':first');
		  }
		  next.children(':first-child').clone().appendTo($(this));

		  for (var i=0;i<4;i++) {
		    next=next.next();
		    if (!next.length) {
		    	next = $(this).siblings(':first');
		  	}
		    
		    next.children(':first-child').clone().appendTo($(this));
		  }
		});
	</script>

	<script type="text/javascript">
		$(function(){
		  $("#nav .dropdown").hover(
		    function() {
		      $('#products-menu.dropdown-menu', this).stop( true, true ).fadeIn("fast");
		      $(this).toggleClass('open');
		    },
		    function() {
		      $('#products-menu.dropdown-menu', this).stop( true, true ).fadeOut("fast");
		      $(this).toggleClass('open');
		    });
		});
	</script>

	<script type="text/javascript">
        $(document).ready(function(){
            $(window).scroll(function() { 
                if ($(document).scrollTop() > 50) { 
                    $(".navbar-fixed-top").css("background-color", "#00838f"); 
                } else {
                $(".navbar-fixed-top").css("background-color", "transparent");
            }
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $(window).scroll(function() { 
                if ($(document).scrollTop() > 50) { 
                    $(".navbar-default .navbar-nav>li>a").css("color", "#fff"); 
                } else {
                $(".navbar-default .navbar-nav>li>a").css("color", "#fff");
            }
            });
        });
    </script>

    <script type="text/javascript">
    	var vid = document.getElementById("myVideo"); 
    	$('#myModal').on('hidden.bs.modal', function () {
    	  	vid.pause(); 
    	})
    </script>


	@yield('java_script')
</body>
</html>