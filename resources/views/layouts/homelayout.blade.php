<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ihsbd.net | @yield('title')</title>
	{{-- <meta name="robots" content="noindex"> --}}
	<meta name="description" content="International Hope School Bangladesh">
	<meta name="keywords" content="International Hope School Bangladesh ">
	<meta name="author" content="International Hope School Bangladesh">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta property="og:url" content="http://ihsbd.net/" />
	<meta property="og:site_name" content="Hope School Bangladesh" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<link rel="icon" href="{{ asset('dist/images/icon.png') }}" type="image/png">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<link rel="stylesheet" type="text/css" href="{{ asset('dist/lib/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/lib/animate-css/animate.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/lib/font-awesome/css/font-awesome.min.css') }}">
	<script type="text/javascript" src="{{ asset('dist/lib/jquery/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('dist/lib/bootstrap/js/bootstrap.js') }}"></script>
<!-- 
	<link rel="canonical" href="https://codepen.io/suez/pen/OjGQza?depth=everything&limit=all&order=popularity&page=59&q=icon&show_forks=false" /> -->

	<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/custom.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/mystyle.css') }}">
</head>
<body>

	@include('partials.homenavber')  

 	@yield('content')

 	@include('partials.homefooter')  
	

	<script src='https://cdnjs.cloudflare.com/ajax/libs/react/15.6.1/react.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/react/15.6.1/react-dom.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/classnames/2.2.5/index.min.js'></script>

	<script type="text/javascript" src="{{ asset('dist/js/sliderjs.js') }}"></script>

	<script type="text/javascript">
		(function(){
		  // setup your carousels as you normally would using JS
		  // or via data attributes according to the documentation
		  // https://getbootstrap.com/javascript/#carousel
		  $('#carousel123').carousel({ interval: 2000 });
		  $('#carouselABC').carousel({ interval: 3600 });
		}());

		(function(){
		  $('.carousel-showmanymoveone .item').each(function(){
		    var itemToClone = $(this);

		    for (var i=1;i<4;i++) {
		      itemToClone = itemToClone.next();

		      // wrap around if at end of item collection
		      if (!itemToClone.length) {
		        itemToClone = $(this).siblings(':first');
		      }

		      // grab item, clone, add marker class, add to collection
		      itemToClone.children(':first-child').clone()
		        .addClass("cloneditem-"+(i))
		        .appendTo($(this));
		    }
		  });
		}());
	</script>


	@yield('java_script')
</body>
</html>