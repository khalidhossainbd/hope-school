	<section id="footer">
		<div class="footer-top-icon">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-4" style="border-left: 1px solid #fff">
						<a href="https://www.facebook.com/ihsbd.net/" target="_blank">
						<p class="footer-icon">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</p>
						</a>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4" style="border-left: 1px solid #fff">
						<a href="https://twitter.com/IHSBD1996" target="_blank">
						<p class="footer-icon">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</p>
						</a>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4" style="border-left: 1px solid #fff; border-right:  1px solid #fff">
						<a href="https://www.youtube.com/channel/UCK2HZh3XSCquJ_xFHfNQxBw" target="_blank">
						<p class="footer-icon">
							<i class="fa fa-youtube-play" aria-hidden="true"></i>
						</p>
						</a>
					</div>
					{{-- <div class="col-md-2 col-sm-2 col-xs-2" style="border-left: 1px solid #373491">
						<a href="#">
						<p class="footer-icon">
							<i class="fa fa-google-plus" aria-hidden="true"></i>
						</p>
						</a>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-2" style="border-left: 1px solid #373491">
						<a href="#">
						<p class="footer-icon">
							<i class="fa fa-linkedin" aria-hidden="true"></i>
						</p>
						</a>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-2" style="border-left: 1px solid #373491; border-right:  1px solid #373491">
						<a href="#">
						<p class="footer-icon">
							<i class="fa fa-instagram" aria-hidden="true"></i>
						</p>
						</a>
					</div> --}}
				</div>
			</div>
		</div>
		<div class="footer-bg">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<img class="img-responsive footer-logo" src="{{ asset('dist/images/School_logo.png') }}" alt="logo">
					</div>
					<div class="col-md-3">
						<p class="footer-title-new">Links</p>
						<hr style="margin-top: 0px;">
						<p class="footer-bottom-text">
							<ul class="footer-list">
								<li><a href="{{ url('/') }}">Home Page</a></li>
								<li><a href="http://ihsbd.net/menus/16">Adminssion Infromation</a></li>
								<li><a href="{{ url('/photo_gallery') }}">Photo Gallery</a></li>
								<li><a href="{{ url('/video_gallery') }}">Video Gallery</a></li>
								<li><a href="http://banglaolympiad.org/">Bangla Olympiad</a></li>
							</ul>
						</p>
					</div>
					<div class="col-md-3">
						<p class="footer-title-new">Online Application</p>
						<hr style="margin-top: 0px;">
						<p class="footer-bottom-text">
							<ul class="footer-list">
								<li><a href="{{ url('/contuct_us') }}">Contact Us</a></li>
								<li><a href="">Site Map</a></li>
								<li><a href="http://119.148.19.67:8086/edutech/">Student/Parent Login</a></li>
								<li><a href="http://119.148.19.67:8086/edutech/">Teacher's Login</a></li>
								<li><a href="">Association</a></li>
							</ul>
						</p>
					</div>
					<div class="col-md-3">
						<p class="footer-title-new">Contact Information</p>
						<hr style="margin-top: 0px;">
						<p class="footer-bottom-text">
							International Hope School Bangladesh <br>
							Plot: 7, Road:6, Sector:4 <br>
							Uttara, Dhaka-1230 <br>
							Tel: +880.2.48956999,48953722-3 <br>
							Fax: +880.2.48954242 <br>
							E-mail: info@ihsbd.net <br>
							For Admission: admission@ihsbd.net<br>
						</p>
					</div>
					{{-- <div class="col-md-6 col-md-offset-3">
						<img class="img-responsive footer-logo" src="{{ asset('dist/images/School_logo.png') }}" alt="logo">
					</div>
					<div class="col-md-6 col-md-offset-3">
						<p class="footer-title">International Hope School Bangladesh</p>
						<p class="footer-title-2">Plot:7, Road:6, Sec:4 Uttara, Dhaka-1230 </p>
					</div> --}}
				</div>
			</div>
		</div>

		<div class="footer-bottom" style="border-top: 1px solid #877df0">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						{{-- <p class="footer-bottom-text">Design and Developed by Arks IT </p> --}}
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<p class="footer-bottom-text-2">Copyright © 2013 All Rights Reserved For IHSB</p>
					</div>
				</div>
			</div>
		</div>
	</section>