
	<!-- NAVBAR -->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="brand">
			<a href="{{ url('/kadmin/home') }}"><img src="{{ asset('dist/images/logo.png') }}" alt="Klorofil Logo" class="img-responsive logo" style="max-height: 60px;"></a>
		</div>
		<div class="container-fluid">
			<div class="navbar-btn">
				<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
			</div>
			{{-- <form class="navbar-form navbar-left">
				<div class="input-group">
					<input type="text" value="" class="form-control" placeholder="Search dashboard...">
					<span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
				</div>
			</form> --}}
			
			<div id="navbar-menu">
				<ul class="nav navbar-nav navbar-right">
					
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="lnr lnr-question-circle"></i> <span>Help</span></a>
						{{-- <ul class="dropdown-menu">
							<li><a href="#">Basic Use</a></li>
							<li><a href="#">Working With Data</a></li>
							<li><a href="#">Security</a></li>
							<li><a href="#">Troubleshooting</a></li>
						</ul> --}}
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{ asset('assets/img/user.png') }}" class="img-circle" alt="Avatar"> <span>{{ Auth::user()->name }}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
						<ul class="dropdown-menu">
							<li><a href="#"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
							<li><a href="#"><i class="lnr lnr-envelope"></i> <span>Message</span></a></li>
							<li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>
							<li>
								<a href="{{ route('logout') }}"
								    onclick="event.preventDefault();
								             document.getElementById('logout-form').submit();">
								    <i class="lnr lnr-exit"></i> <span>Logout</span></a>
								

								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								    {{ csrf_field() }}
								</form>
							</li>
						</ul>
					</li>
					<!-- <li>
						<a class="update-pro" href="https://www.themeineed.com/downloads/klorofil-pro-bootstrap-admin-dashboard-template/?utm_source=klorofil&utm_medium=template&utm_campaign=KlorofilPro" title="Upgrade to Pro" target="_blank"><i class="fa fa-rocket"></i> <span>UPGRADE TO PRO</span></a>
					</li> -->
				</ul>
			</div>
		</div>
	</nav>
	<!-- END NAVBAR -->

	<!-- LEFT SIDEBAR -->
	<div id="sidebar-nav" class="sidebar">
		<div class="sidebar-scroll">
			<nav>
				<ul class="nav">
					<li><a href="{{ url('kadmin/home') }}" class="active"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
					<li><a href="#" class=""><i class="lnr lnr-cog"></i> <span>User Control</span></a></li>

					<li>
						<a href="#subPages1" data-toggle="collapse" class="collapsed"><i class="lnr lnr-magic-wand"></i><span>Menu</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
						<div id="subPages1" class="collapse ">
							<ul class="nav">
								<li><a href="{{ url('kadmin/menus') }}" class="">Menu List</a></li>
								<li><a href="{{ url('kadmin/menus/create') }}" class="">Add New Sub Menu</a></li>
								<li><a href="{{ url('kadmin/menu-images') }}" class="">Menu Images</a></li>
							</ul>
						</div>
					</li>
					
					<li>
						<a href="#subPages2" data-toggle="collapse" class="collapsed"><i class="lnr lnr-chart-bars"></i><span>Branches</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
						<div id="subPages2" class="collapse ">
							<ul class="nav">
								<li><a href="{{ url('kadmin/branches') }}" class="">Branch List</a></li>
								<li><a href="{{ url('kadmin/branches-activity') }}" class="">Branch Activity List</a></li>
								<li><a href="{{ url('kadmin/branches-images') }}" class="">Branch Images</a></li>
							</ul>
						</div>
					</li>

					<li>
						<a href="#subPages6" data-toggle="collapse" class="collapsed"><i class="lnr lnr-linearicons"></i><span>Gallery</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
						<div id="subPages6" class="collapse ">
							<ul class="nav">
								<li><a href="{{ url('kadmin/sliders') }}" class="">Main Slider</a></li>
								<li><a href="{{ url('kadmin/photo-gallery') }}" class="">Photo Gallery </a></li>
								<li><a href="{{ url('kadmin/video-gallery') }}" class=""> Video Gallery </a></li>
							</ul>
						</div>
					</li>
					
					{{-- <li><a href="" class=""><i class="lnr lnr-alarm"></i> <span>Email</span></a></li> --}}

					<li>
						<a href="#subPages3" data-toggle="collapse" class="collapsed"><i class="lnr lnr-code"></i> <span>Campus Life</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
						<div id="subPages3" class="collapse ">
							<ul class="nav">
								<li><a href="{{ url('kadmin/campuslife') }}" class="">Features List</a></li>
								<li><a href="{{ url('kadmin/campuslife/create') }}" class="">Add New Feature</a></li>
								<li><a href="{{ url('kadmin/campus-images') }}" class="">Features Images</a></li>
							</ul>
						</div>
					</li>

					<li>
						<a href="#subPages4" data-toggle="collapse" class="collapsed"><i class="lnr lnr-chart-bars"></i><span>Facilities</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
						<div id="subPages4" class="collapse ">
							<ul class="nav">
								<li><a href="{{ url('kadmin/facilities') }}" class="">Facilities list</a></li>
								<li><a href="{{ url('kadmin/facilities/create') }}" class="">Add new facility</a></li>
								<li><a href="{{ url('kadmin/facility-images') }}" class="">Facilities Images</a></li>
							</ul>
						</div>
					</li>


					{{-- <li><a href="" class=""><i class="lnr lnr-chart-bars"></i><span>Panels</span></a></li> --}}
					<li>
						<a href="#subPages5" data-toggle="collapse" class="collapsed"><i class="lnr lnr-dice"></i> <span>Events</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
						<div id="subPages5" class="collapse ">
							<ul class="nav">
								<li><a href="{{ url('kadmin/events') }}" class="">Events List</a></li>
								<li><a href="{{ url('kadmin/events/create') }}" class="">Add new event</a></li>
								<li><a href="{{ url('kadmin/events-image') }}" class="">Event Images</a></li>
							</ul>
						</div>
					</li>
					<li>
						<a href="#subPages55" data-toggle="collapse" class="collapsed"><i class="lnr lnr-linearicons"></i> <span>Sports</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
						<div id="subPages55" class="collapse ">
							<ul class="nav">
								<li><a href="{{ url('kadmin/sports') }}" class="">Sports Events List</a></li>
								<li><a href="{{ url('kadmin/sports/create') }}" class="">Add new Sport event</a></li>
								<li><a href="{{ url('kadmin/sports-images') }}" class="">Sports Images</a></li>
							</ul>
						</div>
					</li>
					<li>
						<a href="#subPages66" data-toggle="collapse" class="collapsed"><i class="lnr lnr-linearicons"></i> <span>Bangla Olympiad</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
						<div id="subPages66" class="collapse ">
							<ul class="nav">
								<li><a href="{{ url('kadmin/bangla-olympiad') }}" class="">Bangla Olympiad Events List</a></li>
								<li><a href="{{ url('kadmin/bangla-olympiad/create') }}" class="">Add new Bangla Olympiad event</a></li>
								<li><a href="{{ url('kadmin/olympiad-images') }}" class="">Bangla Olympiad Images</a></li>
							</ul>
						</div>
					</li>
					
					{{-- <li><a href="tables.html" class=""><i class="lnr lnr-dice"></i> <span>Tables</span></a></li> --}}
					{{-- <li><a href="typography.html" class=""><i class="lnr lnr-text-format"></i> <span>Typography</span></a></li>
					<li><a href="icons.html" class=""><i class="lnr lnr-linearicons"></i> <span>Icons</span></a></li> --}}
				</ul>
			</nav>
		</div>
	</div>
	
	<!-- END LEFT SIDEBAR -->