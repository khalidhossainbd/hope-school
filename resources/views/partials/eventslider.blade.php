<div class="container">
	<div class="row">
        <div class="col-md-6">
            <p class="head-one" style="margin-top:40px;">Events & Media</p>
            <hr class="event-hr" width="60%">
        </div>
    </div>
	<div class="row">
		<section class="customer-logos slider">
			@foreach($myevents as $item)
				<div class="slide" style="width: 200px;"><img style="width: 200px;" src="{{ asset('uploads/events/'.$item->image) }}"></div>
			@endforeach
      
   		</section>
	</div>
</div>