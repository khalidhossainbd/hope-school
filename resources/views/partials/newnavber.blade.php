	{{-- navber section start --}}
	<section>
		<div class="">
			<nav class="navbar navbar-default navbar-fixed-top">
			  <div class="container">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand" href="#">
			      	<img class="img-responsive top-logo" src="{{ asset('dist/images/logo.png') }}" alt="Logo">
			      </a>
			    </div>

			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav navbar-right">
			        <li class="dropdown">
			            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About IHSB<span class="caret"></span></a>
			            <ul class="dropdown-menu animate__animated animate__fadeInRight">
			                @php
			                    $num1 = count($menuone);
			                    $i = 0;
			                @endphp
			                @foreach($menuone as $item)
			                    @if(++$i === $num1)
			                    <li><a href="{{ url('/'.$item->url_name) }}">{{ $item->submenu }}</a></li>
			                    @else
			                    <li><a href="{{ url('/'.$item->url_name) }}">{{ $item->submenu }}</a></li>
			                    <li role="separator" class="divider"></li>
			                    @endif
			                @endforeach
			            </ul>
			        </li>
			        <li class="dropdown">
			            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Branches<span class="caret"></span></a>
			            <ul class="dropdown-menu animate__animated animate__fadeInRight">
			                @php
			                    $numItems = count($branches);
			                    $i = 0;
			                @endphp
			                @foreach($branches as $item)
			                    @if(++$i === $numItems)
			                    <li><a href="{{ url('/branches/'.$item->url_name) }}">{{ $item->title }}</a></li>
			                    {{-- <li role="separator" class="divider"></li> --}}
			                    @else
			                    <li><a href="{{ url('/branches/'.$item->url_name) }}">{{ $item->title }}</a></li>
			                    <li role="separator" class="divider"></li>
			                    @endif
			                @endforeach
			                
			            </ul>
			        </li>
			        <li class="dropdown">
			            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Achievements<span class="caret"></span></a>
			            <ul class="dropdown-menu animate__animated animate__fadeInRight">
			                <li><a href="{{ url('/sports') }}">Sports</a></li>
			                <li role="separator" class="divider"></li>
			                <li><a href="{{ url('/olympiads') }}">Bangla Olympiad</a></li>
			                <li role="separator" class="divider"></li>
			                @php
			                    $num2 = count($menutwo);
			                    $i = 0;
			                @endphp
			                @foreach($menutwo as $item)
			                    @if(++$i === $num2)
			                    <li><a href="{{ url('/'.$item->url_name) }}">{{ $item->submenu }}</a></li>
			                    @else
			                    <li><a href="{{ url('/'.$item->url_name) }}">{{ $item->submenu }}</a></li>
			                    <li role="separator" class="divider"></li>
			                    @endif
			                @endforeach
			            </ul>
			        </li>
			        <li class="dropdown">
			            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Academics<span class="caret"></span></a>
			            <ul class="dropdown-menu animate__animated animate__fadeInRight">
			                @php
			                    $num3 = count($menuthree);
			                    $i = 0;
			                @endphp
			                @foreach($menuthree as $item)
			                    @if(++$i === $num3)
			                    <li><a href="{{ url('/'.$item->url_name) }}">{{ $item->submenu }}</a></li>
			                    @else
			                    <li><a href="{{ url('/'.$item->url_name) }}">{{ $item->submenu }}</a></li>
			                    <li role="separator" class="divider"></li>
			                    @endif
			                @endforeach
			            </ul>
			        </li>
			        <li class="dropdown">
			            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admission<span class="caret"></span></a>
			            <ul class="dropdown-menu animate__animated animate__fadeInRight">
			                @php
			                    $num4 = count($menufour);
			                    $i = 0;
			                @endphp
			                @foreach($menufour as $item)
			                    @if(++$i === $num4)
			                    <li><a href="{{ url('/'.$item->url_name) }}">{{ $item->submenu }}</a></li>
			                    @else
			                    <li><a href="{{ url('/'.$item->url_name) }}">{{ $item->submenu }}</a></li>
			                    <li role="separator" class="divider"></li>
			                    @endif
			                @endforeach
			            </ul>
			        </li>
			        <li class="dropdown">
			            <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gallery<span class="caret"></span></a>
			            <ul class="dropdown-menu animate__animated animate__fadeInRight">
			                <li><a href="{{ url('/photo_gallery') }}">Photo Gallery</a></li>
			                <li role="separator" class="divider"></li>
			                <li><a href="{{ url('/video_gallery') }}">Video Gallery</a></li>
			                {{-- <li role="separator" class="divider"></li> --}}
			            </ul>
			        </li>
			        <li class="dropdown">
			            <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Login<span class="caret"></span></a>
			            <ul class="dropdown-menu animate__animated animate__fadeInRight">
			                <li><a href="http://119.148.19.67:8086/edutech/">Student/Parent Login</a></li>
			                <li role="separator" class="divider"></li>
			                <li><a href="http://119.148.19.67:8086/edutech/">Teacher's Login</a></li>
			                {{-- <li role="separator" class="divider"></li> --}}
			            </ul>
			        </li>
			      </ul>
			    </div>
			  </div>
			</nav>
		</div>
	</section>
	{{-- navber section end --}}