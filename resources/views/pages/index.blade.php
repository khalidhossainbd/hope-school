@extends('layouts.mainlayout')

@section('title', 'Home Page')

@section('content')


<!-- Start WOWSlider.com BODY section -->
{{-- <div id="wowslider-container1">
<div class="ws_images">
    <ul>
        
        @php
            $i=0;
        @endphp
        @foreach($myslider as $popimg)
            <li><img src="{{ asset('uploads/sliders/'.$popimg->images) }}" alt="sliderone" title="sliderone" id="wows1_{{$i}}"/></li>
            @php  $i++; @endphp
        @endforeach
    </ul>
</div>
</div>  
<script type="text/javascript" src="{{ asset('dist/plugin/engine1/wowslider.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/plugin/engine1/script.js') }}"></script> --}}
<!-- End WOWSlider.com BODY section -->



{{-- <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> --}}
  <!-- Indicators -->
  {{-- <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol> --}}

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    @php
        $i=1;
    @endphp
    @foreach($myslider as $popimg)
        @if($i == 1)
        <div class="item active">
            <picture>
                <source class="img-responsive animate__animated animate__fadeInRight" media="(max-width:768px)" srcset="https://dummyimage.com/768x600/000/fff">
                <img class="img-responsive animate__animated animate__fadeInRight" src="{{ asset('uploads/sliders/'.$popimg->images) }}" alt="Slider Images">
            </picture>
        </div>

        @else
        <div class="item">
            <picture>
                <source class="img-responsive animate__animated animate__zoomIn" media="(max-width:768px)" srcset="https://dummyimage.com/768x600/000/fff">
                <img class="img-responsive animate__animated animate__zoomIn" src="{{ asset('uploads/sliders/'.$popimg->images) }}" alt="Slider Images">
            </picture>
        </div>
        @endif
        @php  $i++; @endphp
    @endforeach
  </div>

  <!-- Controls -->
  {{-- <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> --}}
{{-- </div> --}}



{{-- 
<video autoplay loop controls width="100%">
  <source src="{{ asset('dist/img/video_one.m4v') }}" type="video/mp4">
</video> --}}

{{-- <section  height="600" style="overflow: hidden;">
<video class="video-fluid z-depth-1" autoplay loop no-controls muted width="100%">
  <source src="{{ asset('dist/img/video.mp4') }}" type="video/mp4" />
</video>
</section> --}}

    <section style="margin: 25px 0">
        <div >
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <a class="btn btn-success btn-lg btn-block my-new-btn" target="_blank" href="http://119.148.19.67:8086/edutech/edutechsadmin">
                            Online Payment System
                        </a>
                        
                    </div>
                    <div class="col-md-8">
                        <p class="head-new-one">"Please Log In For Online Payment System"</p>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

  

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="head-one" style="margin-top:40px;">Campus Life</p>
                    <hr class="event-hr" width="80%">
                </div>
            </div>
            <div class="row" style="margin:30px 0;">
                @foreach($campus as $item)
                <div class="col-md-3">
                    <div class="media">
                      <div class="media-left">
                        <img alt="life" src="{{ asset('uploads/campus/'.$item->image) }}" class="media-object" style="width:60px">
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading">{{ $item->title }}</h4>
                        <p class="media-text">{{ $item->subtitle }}</p>
                        
                        <p><a href="{{ url('/campus-life/'.$item->id) }}">....read more</a></p>
                        
                      </div>
                    </div>
                </div>
                @endforeach
                
            </div>
            
        </div>
    </section>

    <section>
        <div style="background-color: #107e6d;">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <div class="mar-div">
                            <p style="padding: 15px ; color: green; text-align: center; font-size: 22px; margin: 0px; font-weight: 600;">ADMISSION</p>
                        </div>
                        
                    </div>
                    <div class="col-md-10">
                        <marquee  direction="left" onmouseover="this.stop();" onmouseout="this.start();">
                            <p class="mar-text">*** Welcome To International Hope School Bangladesh</p>
                        </marquee>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

    <section style="margin-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="head-one" style="margin-top:40px;">Facilities</p>
                    <hr class="event-hr" width="60%">
                </div>
            </div>
            <div class="row">
                @foreach($myfacility as $items)
                <div class="col-md-4 col-sm-6" style="padding:15px;">
                    <div class="box19">
                        <img src="{{ asset('uploads/facility/'.$items->image)}}" alt="Facilities" class="img-responsive">
                        <div class="box-content">
                            <a href="{{ url('school-facility/'.$items->id) }}" style="text-decoration: none;">
                            <h3 class="title">{{ $items->title }}</h3>
                            <p class="sub-title">{{ $items->subtitle }}</p>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            
        </div>
    </section>

    <section class="section-bg">
        {{-- @include('partials.eventslider')   --}}
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="head-one" style="margin-top:40px;">Events & Media</p>
                    <hr class="event-hr" width="60%">
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div id="exTab1" class="">  
                        <ul  class="nav nav-pills">
                            <li class="active">
                            <a  href="#1a" data-toggle="tab">Events</a>
                            </li>
                            <li><a href="#2a" data-toggle="tab">Notices</a>
                            </li>
                        </ul>

                        <div class="tab-content clearfix">
                            <div class="tab-pane active" id="1a" style="max-height: 400px; overflow: hidden; padding: 10% 0;">
                                <section id="carousel">   
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
                                              <ol class="carousel-indicators">
                                                @php $x=1; @endphp
                                                @foreach($myevents as $item)

                                                @if($x==1)
                                                <li data-target="#fade-quote-carousel" data-slide-to="{{$x}}" class="active"></li>
                                                @endif
                                                <li data-target="#fade-quote-carousel" data-slide-to="{{$x}}"></li>
                                                @php $x++; @endphp
                                                @endforeach
                                               
                                              </ol>
                                              <div class="carousel-inner">
                                                @php $x=1; @endphp
                                                @foreach($myevents as $item)

                                                @if($x==1)
                                                <div class="item active">
                                                    
                                                    <div class="profile-circle" style="background-color: rgba(0,0,0,.2);">
                                                        <img class="media-object img-thumbnail" src="{{ asset('uploads/events/'.$item->image) }}" alt="" style="max-height: 100px;">
                                                    </div>
                                                    <blockquote>
                                                        <h4 class="media-heading">{{ $item->title }}</h4>
                                                        <p>{{ $item->subtitle }}<br> <a href="{{ url('school-events/'.$item->id) }}">Details.....</a></p>
                                                    </blockquote>   
                                                </div>
                                                @endif
                                                <div class="item">
                                                    
                                                    <div class="profile-circle" style="background-color: rgba(0,0,0,.2);">
                                                        <img class="media-object img-thumbnail" src="{{ asset('uploads/events/'.$item->image) }}" alt="" style="max-height: 100px;">
                                                    </div>
                                                    <blockquote>
                                                        <h4 class="media-heading">{{ $item->title }}</h4>
                                                        <p>{{ $item->subtitle }}<br> <a href="{{ url('school-events/'.$item->id) }}">Details.....</a></p>
                                                    </blockquote> 
                                                </div>
                                                @php $x++; @endphp
                                                @endforeach
                                                
                                              </div>
                                            </div>
                                        </div>                          
                                    </div>
                                    
                                </section>
                            </div>
                            <div class="tab-pane" id="2a">
                                <h3>No data Found</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="video-bg">
                            <a href="" style="text-decoration: none;" data-toggle="modal" data-target="#exampleModalCenter">
                                <div class="sonar-wave"></div>
                                <div class="video-btn">
                                    <p class="video-btn-text">Video</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="box">
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="background-color: transparent;">
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> --}}
                  <div class="modal-body">
                    <div class="embed-responsive embed-responsive-16by9">        
                        {{-- <iframe class="embed-responsive-item" width="100%" src="https://www.youtube.com/embed/gw1Ek6rBvr0?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> --}}
                        <iframe id="myVideo" src="https://drive.google.com/file/d/1ig5To6TqEm97WKEt4war4qOm845fYnUZ/preview" width="100%"></iframe>
                    </div>  
                  </div>
                    
                </div>
              </div>
            </div>
        </div>
    </section>

    <section style="margin:50px 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                <p class="head-one">Associates</p>
                <hr class="event-hr" width="60%">
                </div>
            </div>
            

            <div class="col-md-12">
                <div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="3000" id="myCarousel">
                  <div class="carousel-inner">
                    <div class="item active">
                      <div class="col-md-2 col-sm-6 col-xs-12">
                        <a href="#">
                            <img src="{{ asset('dist/img/logo/cambridge-1.jpg') }}" class="img-responsive" alt="Other Logo">
                        </a>
                    </div>
                    </div>
                    <div class="item">
                      <div class="col-md-2 col-sm-6 col-xs-12">
                        <a href="#">
                            <img src="{{ asset('dist/img/logo/british.jpg') }}" class="img-responsive" alt="Other Logo">
                        </a>
                    </div>
                    </div>
                    <div class="item">
                      <div class="col-md-2 col-sm-6 col-xs-12"><a href="#"><img src="{{ asset('dist/img/logo/edexcel.jpg') }}" class="img-responsive" alt="Other Logo"></a></div>
                    </div>
                    <div class="item">
                      <div class="col-md-2 col-sm-6 col-xs-12"><a href="#"><img src="{{ asset('dist/img/logo/sat.jpg') }}" class="img-responsive" alt="Other Logo"></a></div>
                    </div>
                    <div class="item">
                      <div class="col-md-2 col-sm-6 col-xs-12"><a href="#"><img src="{{ asset('dist/img/logo/EduCan.jpg') }}" class="img-responsive" alt="Other Logo"></a></div>
                    </div>
                    <div class="item">
                      <div class="col-md-2 col-sm-6 col-xs-12"><a href="#"><img src="{{ asset('dist/img/logo/pioneer.jpg') }}" class="img-responsive" alt="Other Logo"></a></div>
                    </div>
                    <div class="item">
                      <div class="col-md-2 col-sm-6 col-xs-12"><a href="#"><img src="{{ asset('dist/img/logo/cambridge-1.jpg') }}" class="img-responsive" alt="Other Logo"></a></div>
                    </div>
                    <div class="item">
                      <div class="col-md-2 col-sm-6 col-xs-12"><a href="#"><img src="{{ asset('dist/img/logo/sat.jpg') }}" class="img-responsive" alt="Other Logo"></a></div>
                    </div>
                  </div>
                 
                </div>
            </div>
        </div>
    </section>

   


@endsection


@section('java_script')
   
@endsection