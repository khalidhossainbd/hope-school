@extends('layouts.homelayout')

@section('title', 'Videos Gallery')

@section('content')



	<div class="top-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<p class="page-head">Videos Gallery</p>
				</div>
			</div>
			
		</div>
	</div>

	

	<div class="container gal-container">
		@foreach($myvideo as $item)
			<div class="col-md-4 col-sm-6 co-xs-12 gal-item">
				<p style="text-align: center;">{{ $item->title }}</p>
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#{{ $item->id }}">
		          <img src="{{ asset('uploads/videoimg/'.$item->video_img) }}" class="img-responsive" style="max-height: 300px;">
		        </a>
		        <div class="modal fade" id="{{ $item->id }}" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content" style="background-color: transparent;">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                
		                <div class="embed-responsive embed-responsive-16by9">        
		                    <iframe class="embed-responsive-item" width="100%" src="
		                    {{ $item->video_url }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		                </div>  
		              </div>
		                
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		@endforeach
	</div>



@endsection


@section('java_script')



@endsection
