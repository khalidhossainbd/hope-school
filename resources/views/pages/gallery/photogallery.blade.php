@extends('layouts.homelayout')

@section('title', 'Photo Gallery')

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="page-head">Photo Gallery</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container">
	@foreach($photos as  $items)
	<p class="page-title">{{$items->title}}</p>
	<hr class="event-hr" width="30%">

	<div class="container gal-container">
		@foreach($items->GalleryImage as $myitem)
			<div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#{{ $myitem->id }}">
		          <img src="{{ asset('uploads/photogallery/'.$myitem->images) }}">
		        </a>
		        <div class="modal fade" id="{{ $myitem->id }}" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('uploads/photogallery/'.$myitem->images) }}">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>{{ $myitem->title }}</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		@endforeach
		</div>
	@endforeach
	
</div>



@endsection


@section('java_script')



@endsection
