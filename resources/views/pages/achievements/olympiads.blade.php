@extends('layouts.homelayout')

@section('title', 'Bangla Olympiads')

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="page-head">Bangla Olympiads</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container">
	<div class="row">
		@if(count($olympiad)> 0 )
		@foreach($olympiad as $item)
		<div class="col-md-12">
			<p class="page-title" style="margin-top:10px;">{{ $item->title }}</p>
			<hr class="event-hr" width="60%">
			<div class="page-text" style="margin-bottom: 40px;">
				{!! $item->content !!}
			</div>
			<div class="gal-container">
				@if(count($item->OlympiadImage)>0)
			    	@foreach($item->OlympiadImage as $items)
				    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
				      <div class="box">
				        <a href="#" data-toggle="modal" data-target="#{{ $items->id }}">
				          <img src="{{ asset('uploads/Olympiad/'.$items->images) }}">
				        </a>
				        <div class="modal fade" id="{{ $items->id }}" tabindex="-1" role="dialog">
				          <div class="modal-dialog" role="document">
				            <div class="modal-content">
				                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				              <div class="modal-body">
				                <img src="{{ asset('uploads/Olympiad/'.$items->images) }}">
				              </div>
				                <div class="col-md-12 description">
				                  <h4>This is the third one on my Gallery</h4>
				                </div>
				            </div>
				          </div>
				        </div>
				      </div>
					</div>
					@endforeach
				@else
				<div class="page-text" style="margin-bottom: 40px; text-align: center;">
					== No Image Found ==
				</div>
				@endif
			</div>
		</div>
		@endforeach
		@else
		<div class="page-text" style="margin-bottom: 40px; text-align: center;">
			== No Data Found ==
		</div>
		@endif
	</div>
</div>
@endsection


@section('java_script')



@endsection
