@extends('layouts.homelayout')

@section('title', 'O & A Level')

@section('content')

  

<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<p class="page-head">O & A Level</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container">
	<p class="page-title" style="margin-top:40px;">Achievement of O & A Level</p>
	<hr class="event-hr" width="60%">
	{{-- <p class="page-title">Brief History of International Hope School</p> --}}
	{{-- <hr> --}}
	<p class="page-text" style="margin-bottom: 40px;">
		Our school achieved 97% success in O’ level and 98% in A’ level examinations in May/June 2009.Our students have showed their talents and skills in O’ level and A’ level exams and some of the examples of our school’s promising results are as follows: <br> <br>

		Our student Raqeebul Islam Ketan`s remarkable results are:<br>

		2.      Accounting: 97% <br>

		4.      Chemistry: 94% <br><br>

		In May/June 2008 GCE O-A’ Level Examinations our students got splendid results. 5 of our students scored World’s 7 Highest Marks in May/June, 2008 GCE O-A’ Level Examinations under Edexcel, UK’s largest awarding body.<br> <br>

		
		Here are the splendid results:<br> <br>

		1. Highest mark in the world in GCE O’ Level Physics achieved by Sanjid Shahriar <br>

		3. Highest mark in the world in GCE O’ Level Human Biology achieved by Sanjid Shahriar <br>

		5. Highest mark in the world in GCE AS’ Level Accounting achieved by Rafat Noor Chowdhury 

    <br> <br>


These are:<br> <br>


2. Highest mark in the world in GCE O’ Level Maths B achieved by Subrina Fahreen Eusa<br>

4. Highest mark in the world in GCE AS’ Level Chemistry achieved by Sanjid Shahriar<br>

6. Highest mark in the world in GCE AS’ Level Chemistry achieved by Samiha Maisha<br>

8. Highest mark in the world in GCE A’ Level Accounting achieved by Rafat Noor<br><br>


In May/June 2010 GCE O-A Level Examinations again some of our students scored World’s 12 Highest Marks in May/June, 2010 GCE O-A’ Level Examinations under Edexcel, UK’s largest awarding body.<br><br>

1. Highest mark in the world in GCE O’ Level Chemistry achieved by Sanjeda Khan<br>

3. Highest mark in the world in GCE O’ Level Chemistry achieved by Nabeha Tahrim<br>

5. Highest mark in the world in GCE O’ Level Chemistry achieved by Bulbul Ahmed<br>

7. Highest mark in the world in GCE O’ Level Biology achieved by Najmus Sakib Khan<br>

9. Highest mark in the world in GCE O’ Level Bengali achieved by Tanzina Amin<br>

11. Highest mark in the world in GCE A Level Physics achieved by Sanjid Shahriar

<br><br>
		Last year in 2011, 18 of our students accomplished to be in the list of  Best 10 Highest Achievers in different subjects in May/June, 2011 IGCSE-A’ Level Examinations under Edexcel, UK’s largest awarding body.  Among these 18 students,3 of them became the World Highest Achievers and 3 others  became the Country  Highest Achievers.<br> 

These are:<br>   


World Highest Achievers:<br> <br> 

1.Highest mark in the world in IGCSE,  Biology  achieved by Farhana Islam Farha<br> 

3.Highest mark in the world in IGCSE,  Turkish achieved by Rubaiyat Bin Islam<br> <br> 

Country  Highest Achievers:<br> 

1.Highest mark in the country in IGCSE,  Human Biology  achieved by Rezwan Siddique<br> 

3.Highest mark in the country in IGCSE, FP Mathematics achieved by Rubaiyat Bin Islam  <br> <br>          



After graduation students pursue their higher education in World Class Universities. Some of the universities our students study as follows; Purdeu University, University of Michigan, University of Oklohoma, MIT (USA), UCL(England), Fatih University, Zirve University, METU(Turkey),University of Toronto, Mc Gill University,University of Western Ontorio, Ryerson University, Saint Marry University (Canada), Monash University (Australia)


	</p>
</div>


@endsection





 



 




