@extends('layouts.homelayout')

@section('title', 'O & A Level')

@section('content')

  

<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="page-head">University Enrollment</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container">
	<p class="page-title" style="margin-top:40px;">Enrollment of different University</p>
	<hr class="event-hr" width="60%">
	{{-- <p class="page-title">Brief History of International Hope School</p> --}}
	{{-- <hr> --}}
	<p class="page-text" style="margin-bottom: 40px;">
		IHSB believes that academic preparation is the foundation of university application. <br> <br>

		IHSB takes the responsibility of the students who are eager to build their academic career in the reputable universities . For that reason, we motivate and prepare students for their admission to well known universities. Our experience has suggested that students who excel at school are well-suited for the demands of University academics.<br> <br>

		We are developing students who will make a difference at local and overseas universities and in the world beyond. We expect that these students stand out from the crowd and have a brilliant future ahead.
		<br> <br>
		Some of the Universities where our students have secured admission during 2015:
	</p>

	<table class="table table-bordered">
		<tr>
			<th>Name</th>
			<th>Department</th>
			<th>University</th>
			<th>Country</th>
		</tr>
		<tr>
			<td>Al-Amin Samiur Ashish</td>
			<td>Chemistry</td>
			<td>Erciyes University</td>
			<td>Turkey</td>
		</tr>
		<tr>
			<td>MD. Nakibul Haque Shuvo</td>
			<td>Mechanical Engineering</td>
			<td>Izmir Institute of Technology</td>
			<td>Turkey</td>
		</tr>
		<tr>
			<td>Khondokar Abir</td>
			<td>Industrial Engineering</td>
			<td>Bilkent University</td>
			<td>Turkey</td>
		</tr>
		<tr>
			<td>MD. Monjur Morshed</td>
			<td>Medicine</td>
			<td>Gaziantep University</td>
			<td>Turkey</td>
		</tr>
		<tr>
			<td>Hasan Ahmed</td>
			<td>Mechanical Engineering</td>
			<td>Bilkent University</td>
			<td>Turkey</td>
		</tr>
		<tr>
			<td>Tasnimul Hasan</td>
			<td>Computer Science and Eng.</td>
			<td>Mevlana University</td>
			<td>Turkey</td>
		</tr>
		<tr>
			<td>Yusuf Uyar</td>
			<td>Medicine</td>
			<td>Akdeniz University</td>
			<td>Turkey</td>
		</tr>
		<tr>
			<td>Irtiza Hafiz Purbit</td>
			<td>Computer Science and Eng.</td>
			<td>Georgia Tech</td>
			<td>USA</td>
		</tr>
		<tr>
			<td>Ragib Mostafa</td>
			<td>Computer Science and Eng.</td>
			<td>Rice University</td>
			<td>USA</td>
		</tr>
		<tr>
			<td>Abir Rahman</td>
			<td>Computer Science and Eng.</td>
			<td>North-South University</td>
			<td>Bangladesh</td>
		</tr>
		<tr>
			<td>Abrar Abyaz Bhuiyan</td>
			<td>Law</td>
			<td>London</td>
			<td>England</td>
		</tr>
	</table>
</div>
@endsection





 



 




