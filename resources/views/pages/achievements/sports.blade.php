@extends('layouts.homelayout')

@section('title', 'School Sports')

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<p class="page-head">Sports</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container">
	<div class="row">
		@if(count($sports)> 0 )
		@foreach($sports as $item)
		<div class="col-md-12">
			<p class="page-title" style="margin-top:10px;">{{ $item->title }}</p>
			<hr class="event-hr" width="60%">
			<div class="page-text" style="margin-bottom: 40px;">
				{!! $item->content !!}
			</div>
			<div class="gal-container">
				@if(count($item->SportImage)>0)
		    	@foreach($item->SportImage as $items)
			    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
			      <div class="box">
			        <a href="#" data-toggle="modal" data-target="#{{ $items->id }}">
			          <img src="{{ asset('uploads/sports/'.$items->images) }}">
			        </a>
			        <div class="modal fade" id="{{ $items->id }}" tabindex="-1" role="dialog">
			          <div class="modal-dialog" role="document">
			            <div class="modal-content">
			                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
			              <div class="modal-body">
			                <img src="{{ asset('uploads/sports/'.$items->images) }}">
			              </div>
			                <div class="col-md-12 description">
			                  <h4>This is the third one on my Gallery</h4>
			                </div>
			            </div>
			          </div>
			        </div>
			      </div>
				</div>
				@endforeach
				@endif
			</div>
		</div>
		@endforeach
		@else
		<div class="page-text" style="margin-bottom: 40px; text-align: center;">
			{{-- == No Data Found == --}}
			<img class="img-responsive" src="{{ asset('uploads/sports/sports.jpg') }}">
		</div>
		@endif
	</div>
</div>
@endsection


@section('java_script')



@endsection
