@extends('layouts.homelayout')

@section('title')
	{{ $newmenus->submenu }}
@endsection

@section('content')

<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="page-head">{{ $newmenus->submenu }}</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container" style="margin-bottom: 50px">
	<div class="row">
		{{-- <div class="col-md-5">
			<img src="{{ asset('uploads/pages/'.$newmenus->images) }}" class="img-thumbnail" alt="Images">
		</div> --}}
		<div class="col-md-12">
			<p class="page-title" style="margin-top:10px;">{{ $newmenus->submenu }}</p>
			<hr class="event-hr" width="60%">
			<p class="page-text" style="margin-bottom: 40px;">
				{!! $newmenus->content !!}
			</p>
		</div>
	</div>
</div>


@endsection


@section('java_script')



@endsection
