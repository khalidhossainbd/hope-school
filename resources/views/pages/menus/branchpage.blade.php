@extends('layouts.homelayout')

@section('title')
	{{ $mybranch->title }}
@endsection

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<p class="page-head">{{ $mybranch->title }}</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container">
	<p class="page-title" style="margin-top:40px;">{{ $mybranch->title }} </p>
	<hr class="event-hr" width="60%">
	<div class="page-text" style="margin-bottom: 40px;">
		Address: {!! $mybranch->address !!}
	</div>
	<div class="social-icon">
		<ul>
			<li><a href="{{$mybranch->facebook}}" style="padding:15px 20px" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li><a href="{{$mybranch->youtube}}" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
			<li><a href="{{$mybranch->twitter}}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
		</ul>
	</div>
	
	<div class="page-text" style="margin: 40px 0;">
		{!! $mybranch->content !!}
	</div>

	@if(count($mybranch->Branchactivity)>0)
	
	
	
	@foreach($mybranch->Branchactivity as $oneitem)
		<div class="row">
			<div class="col-md-12">
				<p class="page-title" style="margin-top:40px;">{{ $oneitem->title }}</p>
				<hr class="event-hr" width="40%">
			</div>
		</div>
		
		<div class="gal-container">
			@foreach($oneitem->BranchesImage as $myitem)
				<div class="col-md-4 col-sm-6 co-xs-12 gal-item">
			      <div class="box">
			        <a href="#" data-toggle="modal" data-target="#{{ $myitem->id }}">
			          <img src="{{ asset('uploads/branches/'.$myitem->images) }}">
			        </a>
			        <div class="modal fade" id="{{ $myitem->id }}" tabindex="-1" role="dialog">
			          <div class="modal-dialog" role="document">
			            <div class="modal-content">
			                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
			              <div class="modal-body">
			                <img src="{{ asset('uploads/branches/'.$myitem->images) }}">
			              </div>
			                <div class="col-md-12 description">
			                  <h4>{{ $myitem->title }}</h4>
			                </div>
			            </div>
			          </div>
			        </div>
			      </div>
			    </div>
			@endforeach
		</div>
		
	@endforeach

	@else

	<p>==== No Gallery Images =====</p>

	@endif

{{-- 	<div class="gal-container">


		@foreach($mybranch->BranchesImage as $myitem)
			<div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#{{ $myitem->id }}">
		          <img src="{{ asset('uploads/branches/'.$myitem->images) }}">
		        </a>
		        <div class="modal fade" id="{{ $myitem->id }}" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('uploads/branches/'.$myitem->images) }}">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>{{ $myitem->title }}</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		@endforeach
			
	</div> --}}
</div>


@endsection