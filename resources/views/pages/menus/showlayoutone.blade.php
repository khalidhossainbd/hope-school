@extends('layouts.homelayout')

@section('title')
	{{ $newmenus->submenu }}
@endsection

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="page-head">{{ $newmenus->submenu }}</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container" width="80%">
	<p class="page-title" style="margin-top:40px;">{{ $newmenus->submenu }} of International Hope School</p>
	<hr class="event-hr" width="60%">
	<div class="page-text" style="margin-bottom: 80px;">
		{!! $newmenus->content !!}
	</div>



	@if(count($newmenus->MenuImage)>0)
		<p class="page-title" style="margin-top:40px;">Image Gallery</p>
		<hr class="event-hr" width="20%">
		<div class="gal-container" style="margin-bottom: 50px;">
		@foreach($newmenus->MenuImage as $item)
			<div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#{{$item->id}}">
		          <img src="{{ asset('uploads/pages/'.$item->images) }}">
		        </a>
		        <div class="modal fade" id="{{$item->id}}" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('uploads/pages/'.$item->images) }}">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>{{ $item->title }}</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		@endforeach
		</div>
	@endif
</div>


@endsection