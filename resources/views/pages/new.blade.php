@extends('layouts.newlayout')

@section('title', 'Home Page')

@section('content')


	{{-- slide section start --}}
	<section style="margin-top: 90px;">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			{{-- <ol class="carousel-indicators">
			    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
			</ol> --}}
			<div class="carousel-inner" role="listbox">
			  @php
			      $i=1;
			  @endphp
			  @foreach($myslider as $popimg)
			      @if($i == 1)
			      <div class="item active">
			          {{-- <picture>
			              <source class="img-responsive animate__animated animate__fadeInRight" media="(max-width:768px)" srcset="https://dummyimage.com/768x600/000/fff">
			              <img class="img-responsive animate__animated animate__fadeInRight" src="{{ asset('uploads/sliders/'.$popimg->images) }}" alt="Slider Images">
			          </picture> --}}
			          <img class="img-responsive animate__animated animate__fadeInRight" src="{{ asset('uploads/sliders/'.$popimg->images) }}" alt="Slider Images">
			      </div>

			      @else
			      <div class="item">
			          {{-- <picture>
			              <source class="img-responsive animate__animated animate__zoomIn" media="(max-width:768px)" srcset="https://dummyimage.com/768x600/000/fff">
			              <img class="img-responsive animate__animated animate__zoomIn" src="{{ asset('uploads/sliders/'.$popimg->images) }}" alt="Slider Images">
			          </picture> --}}
			          <img class="img-responsive animate__animated animate__zoomIn" src="{{ asset('uploads/sliders/'.$popimg->images) }}" alt="Slider Images">
			      </div>
			      @endif
			      @php  $i++; @endphp
			  @endforeach
			</div>

		</div>
	</section>
	{{-- slider section end --}}

	
	<section style="margin: 25px 0">
	    <div >
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6">
	                    <a class="btn btn-success btn-lg btn-block my-new-btn" target="_blank" href="https://119.148.19.67/edutech/edutechsadmin">
	                        Online Admission & Renewal
	                    </a>
	                    
	                </div>
	                <div class="col-md-6">
	                    <p class="head-new-one"  style = "text-transform:none;">"Please Log In For Online Admission renewal / Payment System"</p>
	                </div>
	            </div>
	            
	        </div>
	    </div>
	</section>
	
	
	
	<section style="margin: 25px 0">
	    <div >
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6">
	                    <a class="btn btn-success btn-lg btn-block my-new-btn" target="_blank" href="https://119.148.19.67/edutech/Online_Admission.html">
	                        New Students Application
	                    </a>
	                    
	                </div>
	                <div class="col-md-6">
	                    <p class="head-new-one" style = "text-transform:none;">"Please Fill Up & Submit the Form"</p>
	                </div>
	            </div>
	            
	        </div>
	    </div>
	</section>

	{{-- Campus life section start --}}
	<section data-aos="fade-up" data-aos-duration="4000">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-6">
	                <p class="head-one" style="margin-top:40px;">Campus Life</p>
	                <hr class="event-hr">
	            </div>
	        </div>
	        <div class="row" style="margin:30px 0;">
	            @foreach($campus as $item)
	            <div class="col-md-3">
	                <div class="media">
	                  <div class="media-left">
	                    <img alt="life" src="{{ asset('uploads/campus/'.$item->image) }}" class="media-object" style="width:60px">
	                  </div>
	                  <div class="media-body">
	                    <h4 class="media-heading">{{ $item->title }}</h4>
	                    <p class="media-text">{{ $item->subtitle }}</p>
	                    
	                    <p><a href="{{ url('/campus-life/'.$item->id) }}">....read more</a></p>
	                    
	                  </div>
	                </div>
	            </div>
	            @endforeach
	            
	        </div>
	        
	    </div>
	</section>
	{{-- campus life section end --}}

	<section>
	    <div style="background-color: #107e6d;">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-2">
	                    <div class="mar-div">
	                        <p style="padding: 15px ; color: green; text-align: center; font-size: 22px; margin: 0px; font-weight: 600;">ADMISSION</p>
	                    </div>
	                    
	                </div>
	                <div class="col-md-10">
	                    <marquee  direction="left" onmouseover="this.stop();" onmouseout="this.start();">
	                        <p class="mar-text">*** Welcome To International Hope School Bangladesh</p>
	                    </marquee>
	                </div>
	            </div>
	            
	        </div>
	    </div>
	</section>

	{{-- facility section start --}}
	<section style="margin-bottom: 50px;" data-aos="fade-up" data-aos-duration="4000">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-6">
	                <p class="head-one" style="margin-top:40px;">Facilities</p>
	                <hr class="event-hr" width="60%">
	            </div>
	        </div>
	        <div class="row">
	            @foreach($myfacility as $items)
	            <div class="col-md-4 col-sm-6" style="padding:15px;">
	                <div class="box19">
	                    <img src="{{ asset('uploads/facility/'.$items->image)}}" alt="Facilities" class="img-responsive">
	                    <div class="box-content">
	                        <a href="{{ url('school-facility/'.$items->id) }}" style="text-decoration: none;">
	                        <h3 class="title">{{ $items->title }}</h3>
	                        <p class="sub-title">{{ $items->subtitle }}</p>
	                        </a>
	                    </div>
	                </div>
	            </div>
	            @endforeach
	        </div>
	    </div>
	</section>
	{{-- facility section end --}}

	{{-- Event & Media section start --}}
	<section class="section-bg">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-6">
	                <p class="head-one" style="margin-top:40px;">Events & Media</p>
	                <hr class="event-hr" width="60%">
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-sm-8 col-md-8">
	                <div id="exTab1" class="">  
	                    <ul  class="nav nav-pills">
	                        <li class="active">
	                        <a  href="#1a" data-toggle="tab">Events</a>
	                        </li>
	                        <li><a href="#2a" data-toggle="tab">Notices</a>
	                        </li>
	                    </ul>

	                    <div class="tab-content clearfix">
	                        <div class="tab-pane active" id="1a" style="max-height: 400px; overflow: hidden; padding: 10% 0;">
	                            <section id="carousel">   
	                                <div class="row">
	                                    <div class="col-md-12">
	                                        <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
	                                          <ol class="carousel-indicators" style="bottom: -20px">
	                                            @php $x=1; @endphp
	                                            @foreach($myevents as $item)

	                                            @if($x==1)
	                                            <li data-target="#fade-quote-carousel" data-slide-to="{{$x}}" class="active"></li>
	                                            @endif
	                                            <li data-target="#fade-quote-carousel" data-slide-to="{{$x}}"></li>
	                                            @php $x++; @endphp
	                                            @endforeach
	                                           
	                                          </ol>
	                                          <div class="carousel-inner">
	                                            @php $x=1; @endphp
	                                            @foreach($myevents as $item)

	                                            @if($x==1)
	                                            <div class="item active" style="min-height: 200px; padding: 0 25px">
	                                            	<div class="row">
	                                            		<div class="col-sm-4 col-md-4">
	                                            			<img class="media-object img-responsive" src="{{ asset('uploads/events/'.$item->image) }}" alt="Img" >
	                                            		</div>
	                                            		<div class="col-sm-8 col-md-8">
	                                            			<h4 class="media-heading">{{ $item->title }}</h4>
	                                            			<p>{{ $item->subtitle }}<br> <a href="{{ url('school-events/'.$item->id) }}">Details.....</a></p>
	                                            		</div>
	                                            	</div>
	                                            </div>
	                                            @endif
	                                            <div class="item" style="min-height: 200px; padding: 0 25px">
	                                            	<div class="row">
	                                            		<div class="col-sm-4 col-md-4">
	                                            			<img class="media-object img-responsive" src="{{ asset('uploads/events/'.$item->image) }}" alt="img" >
	                                            		</div>
	                                            		<div class="col-sm-8 col-md-8">
	                                            			<h4 class="media-heading">{{ $item->title }}</h4>
	                                            			<p>{{ $item->subtitle }}<br> <a href="{{ url('school-events/'.$item->id) }}">Details.....</a></p>
	                                            		</div>
	                                            	</div>
	                                            </div>
	                                            @php $x++; @endphp
	                                            @endforeach
	                                            
	                                          </div>
	                                        </div>
	                                    </div>                          
	                                </div>
	                                
	                            </section>
	                        </div>
	                        <div class="tab-pane" id="2a">
	                            <h3>No data Found</h3>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="col-sm-4 col-md-4">
	                <div class="row">
	                    <div class="video-bg">
	                        <a href="" style="text-decoration: none;" data-toggle="modal" data-target="#exampleModalCenter">
	                            <div class="sonar-wave"></div>
	                            <div class="video-btn">
	                                <p class="video-btn-text">Video</p>
	                            </div>
	                        </a>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
	{{-- Event & Media section end --}}


	{{-- Associate section start --}}
	<section style="margin:50px 0;">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-6">
	            <p class="head-one">Associates</p>
	            <hr class="event-hr" width="60%">
	            </div>
	        </div>
	        
		<!-- Item slider-->
		<div class="container">

		  <div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12">
		      <div class="carousel carousel-showmanymoveone slide" id="itemslider">
		        <div class="carousel-inner">

		          <div class="item active">
		            <div class="col-xs-12 col-sm-6 col-md-2">
		              <a href="#"><img src="{{ asset('dist/img/logo/cambridge-1.jpg') }}" class="img-responsive center-block"></a>
		              {{-- <h4 class="text-center">MAYORAL SUKNJA</h4>
		              <h5 class="text-center">4000,00 RSD</h5> --}}
		            </div>
		          </div>

		          <div class="item">
		            <div class="col-xs-12 col-sm-6 col-md-2">
		              <a href="#"><img src="{{ asset('dist/img/logo/british.jpg') }}" class="img-responsive center-block"></a>
		             {{--  <h4 class="text-center">MAYORAL KOŠULJA</h4>
		              <h5 class="text-center">4000,00 RSD</h5> --}}
		            </div>
		          </div>

		          <div class="item">
		            <div class="col-xs-12 col-sm-6 col-md-2">
		              <a href="#"><img src="{{ asset('dist/img/logo/edexcel.jpg') }}" class="img-responsive center-block"></a>
		              {{-- <span class="badge">10%</span> --}}
		            </div>
		          </div>

		          <div class="item">
		            <div class="col-xs-12 col-sm-6 col-md-2">
		              <a href="#"><img src="{{ asset('dist/img/logo/sat.jpg') }}" class="img-responsive center-block"></a>
		            </div>
		          </div>

		          <div class="item">
		            <div class="col-xs-12 col-sm-6 col-md-2">
		              <a href="#"><img src="{{ asset('dist/img/logo/EduCan.jpg') }}" class="img-responsive center-block"></a>
		            </div>
		          </div>

		          <div class="item">
		            <div class="col-xs-12 col-sm-6 col-md-2">
		              <a href="#"><img src="{{ asset('dist/img/logo/pioneer.jpg') }}" class="img-responsive center-block"></a>
		            </div>
		          </div>

		        </div>

		        {{-- <div id="slider-control">
		        <a class="left carousel-control" href="#itemslider" data-slide="prev"><img src="https://s12.postimg.org/uj3ffq90d/arrow_left.png" alt="Left" class="img-responsive"></a>
		        <a class="right carousel-control" href="#itemslider" data-slide="next"><img src="https://s12.postimg.org/djuh0gxst/arrow_right.png" alt="Right" class="img-responsive"></a>
		      </div> --}}
		      </div>
		    </div>
		  </div>
		</div>
		<!-- Item slider end-->
	    </div>
	</section>
	{{-- Associate section end --}}

@endsection
