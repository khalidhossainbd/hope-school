@extends('layouts.homelayout')

@section('title', 'School Facilities')

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="page-head">{{ $myfacility->title }}</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container">
	<p class="page-title" style="margin-top:40px;">{{ $myfacility->title }} </p>
	<hr class="event-hr" width="60%">
	<p class="page-text" style="margin-bottom: 40px;">
		{!! $myfacility->subtitle !!}
	</p>
	<p class="page-text" style="margin-bottom: 40px;">
		{!! $myfacility->content !!}
	</p>
	
	@if(!empty($myfacilityimg))
	<p class="page-title" style="margin-top:40px;">Image Gallery</p>
	<hr class="event-hr" width="20%">
	<div class="gal-container">
	@foreach($myfacilityimg as $item)
		<div class="col-md-4 col-sm-6 co-xs-12 gal-item">
	      <div class="box">
	        <a href="#" data-toggle="modal" data-target="#{{ $item->id }}">
	          <img src="{{ asset('uploads/facility/'.$item->images) }}">
	        </a>
	        <div class="modal fade" id="{{ $item->id }}" tabindex="-1" role="dialog">
	          <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	              <div class="modal-body">
	                <img src="{{ asset('uploads/facility/'.$item->images) }}">
	              </div>
	                <div class="col-md-12 description">
	                  <h4>{{ $item->title }}</h4>
	                </div>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	@endforeach
	</div>
	@endif
</div>


@endsection