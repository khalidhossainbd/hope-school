@extends('layouts.homelayout')

@section('title', 'Mission & Vission')

@section('content')
 

<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="page-head">Mission & Vission</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container">
	
	{{-- <p class="page-title">Brief History of International Hope School</p> --}}
	{{-- <hr> --}}

	<div class="row">
		<div class="col-md-5">
			<img src="{{ asset('dist/images/DSC_0358.JPG') }}" class="img-thumbnail" alt="Images">
		</div>
		<div class="col-md-7">
			<p class="page-title" style="margin-top:10px;">Our Mission</p>
			<hr class="event-hr" width="60%">
			<p class="page-text" style="margin-bottom: 40px;">
				Committed to provide dynamic lifelong educational experience through globally relevant and intensive international academic and non-academic programs, acquiring solid competencies in critical thinking, creative problem solving, value judgment, consensus building, intercultural understanding and respect, decision making digital literacy and 21st century skills.
			</p>

			<p class="page-title" style="margin-top:10px;">Our Vission</p>
			<hr class="event-hr" width="60%">
			<p class="page-text" style="margin-bottom: 40px;">
				Developing well-rounded and globally competitive and intellectual individuals.
			</p>

			<p class="page-title" style="margin-top:10px;">To be world class international school with these standardsn</p>
			<hr class="event-hr" width="60%">
			<p class="page-text" style="margin-bottom: 40px;">
				
				<ul>
					<li>International Curriculum</li>
					<li>Stimulating environment</li>
					<li>Academic excellence</li>
					<li>Cultural diversity</li>
					<li>Professional staff from a wide range of nationalities</li>
					<li>Exceptional Staff: Student ratio</li>
					<li>Institutional International Accreditation</li>
					<li>Superb Teaching, Sporting, Extra-mural facilities</li>
					<li>Diverse student population</li>
					<li>Organization works with schools, government and international organizations to develop challenging programs.</li>
				</ul>
			</p>

			<p class="page-title" style="margin-top:10px;">Our Core Values:</p>
			<hr class="event-hr" width="60%">
			<p class="page-text" style="margin-bottom: 40px;">
				Cultivate discipline, self-reliance, responsibility and acceptance of other cultures.
				Our values and attributes, which align with our guiding statements, are integral to creating a school culture and climate to realize our educational goals as follows; </p>

				<table class="table table-bordered">
					<tr>
						<td>Inquiry</td>
						<td>Knowledge</td>
						<td>Principled</td>
					</tr>
				
					<tr>
						<td>Caring</td>
						<td>Balance</td>
						<td>Reflective</td>
					</tr>
					<tr>
						<td>Critical Thinking</td>
						<td>Communication</td>
						<td>Open-mindednesses</td>
					</tr>
					<tr>
						<td>Risk-taking</td>
						<td>Creativity</td>
						<td>Reflection</td>
					</tr>

				</table>
				<br>
			<p class="page-text" style="margin-bottom: 40px;">
				The institution’s mission, vision and values are appropriate to local and international standards, consistent with its all branches operating authority, and implemented in a manner that complies with the Standards of the Cambridge International Examinations and Council of International Schools and Ministry of Education of Bangladesh as well. The institution’s mission gives direction to its activities and provides a basis for the assessment and enhancement of the institution’s effectiveness.
			</p>

		


		</div>

	</div>
	
</div>




@endsection


@section('java_script')



@endsection
