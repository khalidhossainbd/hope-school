@extends('layouts.homelayout')

@section('title', 'Why IHSB')

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<p class="page-head">Why IHSB</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container">
	
	{{-- <p class="page-title">Brief History of International Hope School</p> --}}
	{{-- <hr> --}}

	<div class="row">
		<div class="col-md-5">
			<img src="{{ asset('dist/images/DSC_0358.JPG') }}" class="img-thumbnail" alt="Images">
		</div>
		<div class="col-md-7">
			<p class="page-title" style="margin-top:10px;">Strength of International Hope School</p>
			<hr class="event-hr" width="60%">
			<p class="page-text" style="margin-bottom: 40px;">
				<ul>
					<li>Highly qualified teaching staff.</li>
					<li>Computer based education from PG to A' levels.</li>
					<li>A good moral education from early ages.</li>
					<li>A meeting place for children from 17 different nations and 5 different religions.</li>
					<li>Many sorts of club activities such as English, Music, Debate, Cricket, Football, Chess, etc.</li>
					<li>Science, Physics, Chemistry, Biology and Computer Lab facilities.</li>
					<li>Hostel facility for senior students.</li>
					<li>Student's awareness of their rich Bangladeshi culture and traditions.</li>
					<li>Warm relationship between Parents and Teachers.</li>
					<li>Participation in different International Festivals and Olympiads with many students.</li>
				</ul>
			</p>
		</div>
	</div>
	
</div>

<div class="container">
	<section>
		<div class="container gal-container">
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#3">
		          <img src="{{ asset('dist/images/per_gallery/img1.jpg') }}">
		        </a>
		        <div class="modal fade" id="3" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('dist/images/per_gallery/img1.jpg') }}">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>This is the third one on my Gallery</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#4">
		          <img src="{{ asset('dist/images/per_gallery/img2.jpg') }}">
		        </a>
		        <div class="modal fade" id="4" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('dist/images/per_gallery/img2.jpg') }}">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>This is the fourth one on my Gallery</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#5">
		          <img src="{{ asset('dist/images/per_gallery/img3.jpg') }}">
		        </a>
		        <div class="modal fade" id="5" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('dist/images/per_gallery/img3.jpg') }}">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>This is the fifth one on my Gallery</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#6">
		          <img src="{{ asset('dist/images/per_gallery/img4.jpg') }}">
		        </a>
		        <div class="modal fade" id="6" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('dist/images/per_gallery/img4.jpg') }}">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>This is the sixth one on my Gallery</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#7">
		          <img src="{{ asset('dist/images/per_gallery/img5.jpg') }}">
		        </a>
		        <div class="modal fade" id="7" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('dist/images/per_gallery/img5.jpg') }}">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>This is the seventh one on my Gallery</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#8">
		          <img src="{{ asset('dist/images/per_gallery/img6.jpg') }}">
		        </a>
		        <div class="modal fade" id="8" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('dist/images/per_gallery/img6.jpg') }}">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>This is the eighth one on my Gallery</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#9">
		          <img src="{{ asset('dist/images/per_gallery/img7.jpg') }}">
		        </a>
		        <div class="modal fade" id="9" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('dist/images/per_gallery/img7.jpg') }}">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>This is the ninth one on my Gallery</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		    
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#12">
		          <img src="{{ asset('dist/images/per_gallery/img8.jpg') }}">
		        </a>
		        <div class="modal fade" id="12" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('dist/images/per_gallery/img8.jpg') }}g">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>This is the 12th one on my Gallery</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#13">
		          <img src="{{ asset('dist/images/per_gallery/img1.jpg') }}">
		        </a>
		        <div class="modal fade" id="13" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('dist/images/per_gallery/img1.jpg') }}">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>This is the 13th one on my Gallery</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#14">
		          <img src="{{ asset('dist/images/per_gallery/img2.jpg') }}">
		        </a>
		        <div class="modal fade" id="14" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('dist/images/per_gallery/img2.jpg') }}">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>This is the 14th one on my Gallery</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#15">
		          <img src="{{ asset('dist/images/per_gallery/img3.jpg') }}">
		        </a>
		        <div class="modal fade" id="15" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('dist/images/per_gallery/img3.jpg') }}">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>This is the 15th one on my Gallery</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		    <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
		      <div class="box">
		        <a href="#" data-toggle="modal" data-target="#16">
		          <img src="{{ asset('dist/images/per_gallery/img4.jpg') }}">
		        </a>
		        <div class="modal fade" id="16" tabindex="-1" role="dialog">
		          <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		              <div class="modal-body">
		                <img src="{{ asset('dist/images/per_gallery/img4.jpg') }}">
		              </div>
		                <div class="col-md-12 description">
		                  <h4>This is the 16th one on my Gallery</h4>
		                </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		</div>
	</section>
	
</div>


@endsection


@section('java_script')



@endsection
