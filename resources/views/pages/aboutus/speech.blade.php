@extends('layouts.homelayout')

@section('title', 'Inaugration Speech')

@section('content')


<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="page-head">Inaugration Speech</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container">
	
	{{-- <p class="page-title">Brief History of International Hope School</p> --}}
	{{-- <hr> --}}

	<div class="row">
		<div class="col-md-5">
			<img src="{{ asset('dist/images/DSC_0358.JPG') }}" class="img-thumbnail" alt="Images">
		</div>
		<div class="col-md-7">
			<p class="page-title" style="margin-top:10px;">Inaugration Speech </p>
			<hr class="event-hr" width="60%">
			<p class="page-text" style="margin-bottom: 40px;">
				Mr. Principal,<br>
				Dear Children,<br>
				Ladies and Gentleman,<br><br>

				I have also been deeply touched by the warm welcome accorded to us by the administration and the pupils at this school.<br><br>

				I can observe the cheer and desire to learn in the eyes of the children. The principles and the ideals of Mustafa Kemal Pasa to be the taught to the pupils in this school will, guide them in the future to be good students. After the education and training they receive in this school they will not only be the future leaders of Bangladesh but also play an important role in the advancement of the freindship between Turkey and Bangladesh.<br><br>

				I will always remember the good memories of Bangladesh, I thank you all and I wish you every success in the future.<br><br><br><br>

				 

				 

				9th President of Turkey
			</p>
		</div>
	</div>
	
</div>


@endsection

