@extends('layouts.homelayout')

@section('title', 'Brief History')

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<p class="page-head">Brief History</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container">
	<p class="page-title" style="margin-top:40px;">Brief History of International Hope School</p>
	<hr class="event-hr" width="60%">
	{{-- <p class="page-title">Brief History of International Hope School</p> --}}
	{{-- <hr> --}}
	<p class="page-text" style="margin-bottom: 40px;">
		International Hope School Bangladesh , as a project of Int’l Hope Company Ltd. was established in Dhaka in 1996. The company aims to establish, run and manage residential and non-residential schools, instruction places, nursery schools, and kindergartens, primary schools, secondary schools, technical colleges, vocational schools, or higher educational institutions, universities and similar institutions carrying out all kinds of education and instructions. <br> <br>

		The International Hope Company has been permitted from the Ministry of Education, Bangladesh to set up the proposed school (International Hope School Bangladesh ) and to operate with its revised name and new instructions regarding its operations since 4th April 1996.<br> <br>

		The Int’l Hope Company had constructed two state of the art school buildings on the 2 acres land provided by Bangladeshi Government upon the request of the Chairman of the Company on 28th November 1999 for duration of 99 years.<br> <br>


		International Hope School Bangladesh is one of the leading international schools offering education from Toddler level (age 2) to A Level standard in Bangladesh. The school’s curriculum is based on the British Education System and it is registered with Edexcel dated on 12th August 2004 and British Council to offer International GCSE and A` Level examinations.  The school is also registered under The Ministry of Education of the Government of the People’s Republic of Bangladesh. Though the medium of instruction is in English, students also study the Bangladeshi studies, Bengali Language, Literature, culture and history.   <br> <br>              

		The school has six branches with the total number of 1905 students in Dhaka, Chittagong and Bogra. Currently, 396 staffs are employed at the school including 35 foreign, 210 local teaching faculties.  The student body is culturally diverse and both the Bangladeshi and foreign students study at the school with a population of 89% Bangladeshi and 11% international students from 21 different nations.<br> <br>

		The school has gained a reputation on its academic success by maintaining extraordinary exam results in IGCSE and A Level. On average, 85% of its students achieve top grades (A* to B) in these exams on a regular basis. The graduated students get into prestigious local and international universities across the world including MIT, Stanford, Columbia, Brown, University of Pennsylvania, Rice, and Purdue in the USA, University of Toronto, British Columbia, Mc Gill and York University in Canada, Monash University in Australia, UCL in the UK, Dhaka University in Bangladesh.<br> <br>

		The school is affiliated with the College Board and conducts SAT exams at its premises as one of the few SAT TEST CENTERs in Bangladesh. The school is in partnership with BRITISH COUNCIL and accommodates British Council Teaching Centre at its venues. The school is also in partnership with BRAC University and designs its professional development programs (workshops/ training etc.) in consultation with the university. In addition, the university offers a master program on Educational Leadership and School Improvement for the teachers and head of departments of ITHS.<br> <br>

		IHSB strives to offer its students a setting in which they can progress academically, morally, socially, emotionally and physically. In an effort to provide a well-rounded education, the school aims to provide extracurricular activities including various forms of arts, music, and sports at its purpose built facilities. Various kinds of clubs are organized at the primary and secondary levels to provide students with an opportunity to develop positive interests, activities during their leisure time and have an enthusiasm for serving the school and the community.<br> <br>

		IHSB puts great emphasis on International Olympiads and Project Competitions. The school has garnered so many medals and awards in these Olympiads since its establishment.   In addition, interested students are taken to international events including cultural festivals, sports events, etc. in order to instill the idea of diversity of the world in relation to culture, language and learning to live together.
		<br> <br>

		IHSB strongly encourages students to involve themselves in International fields that will help them develop their full potential and inspire students to set and achieve challenging goals.
	</p>
</div>


@endsection