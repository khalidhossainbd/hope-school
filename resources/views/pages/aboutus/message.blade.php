@extends('layouts.homelayout')

@section('title', 'Chairman Message')

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="page-head">Chairman Message</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container" style="margin-bottom: 50px">


	<div class="row">
		<div class="col-md-5">
			<img src="{{ asset('dist/images/chairman.jpg') }}" class="img-thumbnail" alt="Images">
		</div>
		<div class="col-md-7">
			<p class="page-title" style="margin-top:10px;">Chairman Message of International Hope School</p>
			<hr class="event-hr" width="60%">
			<p class="page-text" style="margin-bottom: 40px;">
				I believe a good school is the holy place where the light of the wisdom will be focused, and the teacher is the magic master of this mysterious laboratory. The true master is the one who will save us from centuries-old pains, and, by the strength of his wisdom, remove the darkness covering our horizons.<br> <br>

				Chairman of Management Committee
			</p>
		</div>
	</div>
	
</div>




@endsection


@section('java_script')



@endsection
