@extends('layouts.homelayout')

@section('title', 'Extra-Curricular Activities')

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<p class="page-head">Extra-Curricular Activities</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container" style="margin-bottom: 50px">


	<div class="row">
		<div class="col-md-4">
			<img src="{{ asset('dist/images/activities.jpg') }}" class="img-thumbnail" alt="Images">
		</div>
		<div class="col-md-8">
			<p class="page-title" style="margin-top:10px;">Extra-Curricular Activities of International Hope School</p>
			<hr class="event-hr" width="60%">
			<p class="page-text" style="margin-bottom: 40px;">
				To enhance the learning environment and extend the development opportunities for each student, Int’l Hope School Bangladesh offers a rich and varied programme of extracurricular activities in academia, the arts and sport. From individual and team sports to art, music and language, activities vary by age and are inspired by the interests of our students. IHSB students enjoy working and playing together.<br> <br>

				Extra-Curricular Activities are available for students from Year 5 through Year 18.  They provide an excellent opportunity for students to round out their education with subjects that may not be typically available to study during the school day.<br> <br>

				Our trips programsdo not only make sure that students broaden their horizons and become global citizens but also respect their culture. The activities are designed to broaden and develop the mind and body. Students have regular opportunities to visit museums, exhibitions and theatres, as well as competitions and tournaments.

				<ul>
					<li class="page-text"><strong>Debating</strong></li>
					<li class="page-text"><strong>Science Week</strong></li>
					<li class="page-text"><strong>Spelling Bee</strong></li>
					<li class="page-text"><strong>Handwriting Competition</strong></li>
					<li class="page-text"><strong>Art Competition</strong></li>
					<li class="page-text"><strong>Idioms Competition</strong></li>
					<li class="page-text"><strong>Fireball Tournament</strong></li>
					<li class="page-text"><strong>Basketball Tournament</strong></li>
					<li class="page-text"><strong>Badminton Tournament</strong></li>
					<li class="page-text"><strong>Kite Day</strong></li>
					<li class="page-text"><strong>Fire Drill</strong></li>
					<li class="page-text"><strong>Victory Day Celebration</strong></li>
					<li class="page-text"><strong>Essay Competition (in Bangla)</strong></li>
					<li class="page-text"><strong>Essay Competition (in Turkish)</strong></li>
					<li class="page-text"><strong>Bangla Olympiad</strong></li>
					<li class="page-text"><strong>Puppet Show</strong></li>
					<li class="page-text"><strong>Amusement Park Visit</strong></li>
					<li class="page-text"><strong>Science Fair</strong></li>
					<li class="page-text"><strong>National Museum Visit</strong></li>
				</ul>
				(Extra-Curricular Activities may vary from one branch to another)
			</p>
		</div>
	</div>
	
</div>




@endsection


@section('java_script')



@endsection
