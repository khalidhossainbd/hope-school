@extends('layouts.homelayout')

@section('title', 'School Clubs')

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="page-head">School Clubs</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container" style="margin-bottom: 50px">


	<div class="row">
		<div class="col-md-4">
			<img src="{{ asset('dist/images/Clubs.png') }}" class="img-thumbnail" alt="Images">
		</div>
		<div class="col-md-8">
			<p class="page-title" style="margin-top:10px;">Clubs of International Hope School</p>
			<hr class="event-hr" width="60%">
			<p class="page-text" style="margin-bottom: 40px;">
				Working closely with friends, having fun and learning new things about yourself, your peers, community and the world can all happen in IBHS Clubs. We are proving through the clubs to have a significant impact on many whole school priorities such as behavior, attendance and attainment. IBHS Clubs also provide a means of developing a sense of belonging, confidence and competence in young people that can translate to other aspects of school life and priorities, such as special educational needs.<br><br>

				 

				Int’l Hope School Bangladesh students participate in a variety of teams and clubs during the school year.  Involvement in these activities does require a time commitment and enthusiasm,nevertheless they are a very rewarding and important part of school life at IBHS. All students are encouraged to make a difference to the school with their ideas and their involvement.  The running of any club or activity is contingent upon student interest. <br><br>

				 

				Our purpose is to provide co-curricular organizations and activities to permit all interested students an opportunity to participate. Every effort is made to avoid membership requirements that would prevent this participation.<br><br>

				 

				There are numerous clubs which focus on sports, art, dance, and other areas to attract active young people and inspire them to participate in a range of alternative clubs. We aim to:
				<ul>
					<li class="page-text">Make use of utilized school facilities more.</li>
					<li class="page-text">Change the attitudes of different aged pupils towards being healthy and active in their everyday lives.</li>
				</ul>
				  
				
			</p>
			<p class="page-text">
				List of the clubs we provide at different branches of Int’l Turkish Hope Schoolis:
				<ul>
				  	<li class="page-text">Art Club</li>
				  	<li class="page-text">Chess Club</li>
				  	<li class="page-text">Cultural Club</li>
				  	<li class="page-text">Dance Club (Bangladeshi)</li>
				  	<li class="page-text">Dance Club(Turkish)</li>
				  	<li class="page-text">Debate Club</li>
				  	<li class="page-text">Drama Club</li>
				  	<li class="page-text">Guidance Club</li>
				  	<li class="page-text">Karate Club</li>
				  	<li class="page-text">Library Club</li>
				  	<li class="page-text">Maths Club</li>
				  	<li class="page-text">Music Club</li>
				  	<li class="page-text">Rubik\'s Cube Club</li>
				  	<li class="page-text">Science Club</li>
				  	<li class="page-text">Sewing and Craft Club</li>
				  	<li class="page-text">Speakers Club</li>
				  	<li class="page-text">Tidiness Club</li>
				  	<li class="page-text">Guitar Club</li>
				  	<li class="page-text">Table Tennis Club</li>
				  	<li class="page-text">Football Club</li>
				  	<li class="page-text">Basketball Club</li>
				  </ul>  
				  (Clubs may vary from one branch to another)
			</p>

		</div>
	</div>
	
</div>




@endsection


@section('java_script')



@endsection
