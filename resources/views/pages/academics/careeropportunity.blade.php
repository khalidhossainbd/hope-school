@extends('layouts.homelayout')

@section('title', 'Career Opportunity')

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="page-head">Career Opportunity</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container" style="margin-bottom: 50px">


	<div class="row">
		<div class="col-md-4">
			<img src="{{ asset('dist/images/career.png') }}" class="img-thumbnail" alt="Images">
		</div>
		<div class="col-md-8">
			<p class="page-title" style="margin-top:10px;">Career Opportunity with International Hope School</p>
			<hr class="event-hr" width="60%">
			<p class="page-text" style="margin-bottom: 40px;">
				International Hope School Bangladesh recruits qualified professionals who are able to serve to the requirements of the students admitted. We have a strong sense of community within the school and we welcome you to be a part of it.
			</p>

			<p class="page-text">
				<table class="table table-bordered">
					<tr>
						<th colspan="2" style="text-align: center;">Music Teacher</th>
					</tr>
					<tr>
						<th>Position</th>
						<td>Music Teacher</td>
					</tr>
					<tr>
						<th>Job Description</th>
						<td>He/She would be required to teach students singing as well as teach them very well about how to play one or more of musical instruments such as Spanish/electronic guitar, keyboard and drums. Also, able to teach the students hands on with musical notes. He/She would be responsible to prepare students to perform on stage on various internal/external occasions. The candidate must be fluent in spoken/written English.</td>
					</tr>
					<tr>
						<th>Application way</th>
						<td>Either can drop the CV at the front desk in the main campus (Address- Plot 7, Road 6, Sector 4, Uttara, Dhaka) ,br> <br>

						CV can also be mailed in the following email addresses: career@ithsbd.net or ithssecretariat@gmail.com</td>
					</tr>
					<tr>
						<th>Facilities</th>
						<td>Attractive remuneration will be given which includes provident fund and two festival bonuses (terms and condition of the contract does apply). Increment occurs every year depending on the factors like teaching performance, students’ observation, collegial conduct etc.</td>
					</tr>
					<tr>
						<th>Last date of application</th>
						<td>15th October 2018</td>
					</tr>
				</table>
			</p>
		</div>
	</div>
	
</div>




@endsection


@section('java_script')



@endsection
