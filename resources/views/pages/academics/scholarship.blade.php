@extends('layouts.homelayout')

@section('title', 'Scholarship')

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="page-head">Scholarship</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container" style="margin-bottom: 50px">


	<div class="row">
		<div class="col-md-4">
			<img src="{{ asset('dist/images/Scholarships.jpg') }}" class="img-thumbnail" alt="Images">
		</div>
		<div class="col-md-8">
			<p class="page-title" style="margin-top:10px;">Scholarship of International Hope School</p>
			<hr class="event-hr" width="60%">
			<p class="page-text" style="margin-bottom: 40px;">
				<strong>
				Any student obtaining 8 A*'s in IGCSE is awarded 75% tuition fee concession.<br> <br>

				Any student obtaining 7 A*'s in IGCSE is awarded 50% tuition fee concession.<br> <br>

				Any student obtaining 6 A*'s in IGCSE is awarded 25% tuition fee concession.<br> <br>

				Any student obtaining 5 A''s at GCE AS Level is awarded  75% tuition fee concession.<br> <br>

				Any student obtaining 4 A''s at GCE AS Level is awarded 50% tuition fee concession.<br> <br>

				Any student obtaining 3 A''s at GCE AS Level is awarded  25% tuition fee concession.<br> <br>

				IHSB also has a scholarship grant policy for the meritorious and underprivilieged students of Bangladesh. School selects these students from various parts of Bangladesh through placement Assesments and interviews..School provides these students with full free accomodation, food and also bears all types of educational expenses.
				</strong>
			</p>
		</div>
	</div>
	
</div>




@endsection


@section('java_script')



@endsection
