@extends('layouts.homelayout')

@section('title', 'Academic Calendar')

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="page-head">Academic Calendar</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container" style="margin-bottom: 50px">


	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<img src="{{ asset('dist/images/academic_calendar_NEW.png') }}" class="img-responsive" alt="Images">
		</div>
		
	</div>
	
</div>




@endsection


@section('java_script')



@endsection
