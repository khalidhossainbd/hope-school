@extends('layouts.homelayout')

@section('title', 'Curriculum')

@section('content')



<div class="top-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<p class="page-head">Curriculum</p>
			</div>
		</div>
		
	</div>
</div>

<div class="container">
	<p class="page-title" style="margin-top:40px;">Curriculum of International Hope School</p>
	<hr class="event-hr" width="60%">
	{{-- <p class="page-title">Brief History of International Hope School</p> --}}
	{{-- <hr> --}}
	<p class="page-text" style="margin-bottom: 40px;">
		The accomplishment of anything worthwhile, whether large or small, depends on the completion of goals, activities and milestones.Every successful concept and project in life requires a proper framework and planning. This relates to all processes, including education. Since a good education is the fundamental matter for the development of character, values and morals, Int’l Hope School Bangladesh is proud of its strong academic tradition. <br> <br> 

		IHSB believes that the improvement of the children is possible only with a properly designed curriculum and syllabus from the play stage and thereby lays such a curriculum-path that in inter-connected through different levels.<br> <br>

		Pupils will be prepared for the International General Certificate of Secondary Education (IGCSE) and General Certificate Examination (GCE) held by Edexcel International Examinations Board. The academic calendar and curriculum are up-to-date in accordance with International GCSE /A\' Level courses. Our curriculum offers a complete learning package program to meet the international standard.<br> <br>

 
		Bengali language classes are compulsory for Bangladeshi citizens and for the students whose father or mother is originally Bangladeshi.Students are classified based on their origin and not the passport inthe regard.<br> <br>

		Students staying abroad from a long time may be given special permission to study at foundation level but they are advised to attain the standard appealing their grades as quickly as possible.<br> <br>

		For the greater benefit of the students, Turkish language will be taught from class 3 to 8. It is well known that this post-modern era knowing any foreign language offers students an extra advantage.<br><br>
		Religious Studies is not compulsory for the non-Muslim students.
	</p>

	<p class="page-text">
		Int’l Hope School Bangladesh separates the school according to the different year groups:<br><br>

		<table class="table table-bordered">
			<tr>
				<th>Preschool Section</th>
				<td>Toddler,Playgroup, Nursery, Kindergarten 1, Kindergarten 2</td>
			</tr>
			<tr>
				<th>Junior Section</th>
				<td>Grades I, II, III, IV, V</td>
			</tr>
			<tr>
				<th>Senior Section</th>
				<td>Grades VI, VII, VIII</td>
			</tr>
			<tr>
				<th>O Level</th>
				<td>Grades IX, X</td>
			</tr>
			<tr>
				<th>A Level</th>
				<td>Grades XI, XII</td>
			</tr>
		</table>
	</p>
	<p class="page-text">
		Students are assessed by grades, as follows:<br> <br>

		<table class="table table-bordered">
			<tr>
				<th>A= 90 – 100% </th>
				<th>Excellent</th>
			</tr>
			<tr>
				<th>B= 80 – 89%</th>
				<th>Very Good</th>
			</tr>
			<tr>
				<th>C= 70 – 79% </th>
				<th>Good</th>
			</tr>
			<tr>
				<th>D= 60 – 69%</th>
				<th>Fair</th>
			</tr>
			<tr>
				<th>E= 50 – 59%</th>
				<th>Needs Improvement</th>
			</tr>
			<tr>
				<th>F= 0 –49%</th>
				<th>Fail</th>
			</tr>
		</table>		                
	</p>
	<p class="page-text">
		<strong>Curriculum: </strong><br>

		The curriculum is carefully designed for children, not only to fulfill the requirements and commitments but also to reach the target of our motto. <br> <br>

		 

		<strong>Playgroup Subjects Taught: </strong><br>

		Basic Skills, Storytelling, English (Writing), Number (Writing), Coloring, Drawing, Painting, Computer English, Cut & Past, Rhymes and Craft Work.<br> <br>

		 

		<strong>Nursery Subjects Taught: </strong><br>

		Vocabulary, Number Work, English, Coloring, Drawing, Cut & Past, Craft Work, Seesaw, Manner, PE/Games, Computer English, Storytelling, Rhymes. <br> <br>

		 

		<strong>Kindergarten Subjects Taught:</strong><br>

		Mathematics, Seesaw, Oxford Reading Circle, Basic English, Computer English, Manner, Bengali, Science, Music, Art, Physical Exercise, Story Telling.<br> <br>

		 

		<strong>Subjects taught in Junior Section:</strong><br>

		Art, Bengali Language, Bengali Literature, Computer Studies, English Language, English Literature, Grammar, Mathematics, Moral Values, Music, Physical Exercise, Religious Studies, Science, Social Studies, Spoken, Turkish Language, Writing.<br> <br>

		During the academic year, numerous field trips and extracurricular activities are conducted to complement the curriculum being taught in class.<br> <br>

		 

		<strong>Subjects taught in Senior Section:</strong><br>

		Bengali Language, Bengali Literature, Biology, Chemistry, Computer Studies ,English Language, English Language 2, English Literature , Geography, History, Mathematics, Moral Values, Physical Exercise, Physics, Religious Studies, Turkish Language
	</p>
	<p class="page-text">
		<strong>Subjects offered for the \"O\" Level (International GCSE) are as follows:</strong> <br>

		<table class="table table-bordered">
			<tr>
				<th>Accounting</th>
				<th>English Language B</th>
				<th>Bengali Language</th>
			</tr>
			<tr>
				<th>Mathematics A</th>
				<th>Biology</th>
				<th>Human Biology</th>
			</tr>
			<tr>
				<th>Business Studies</th>
				<th>Mathematics B</th>
				<th>Chemistry</th>
			</tr>
			<tr>
				<th>Physics</th>
				<th>Commerce</th>
				<th>Pure Mathematics</th>
			</tr>
			<tr>
				<th>Computer Studies</th>
				<th>Turkish</th>
				<th>Economics</th>
			</tr>
		</table>
	</p>
	<p class="page-text">
		<strong>Subjects offered for the \"A\" Level (GCE) are as follows:</strong>
	</p>
</div>


@endsection