$(document).ready(function () {
$('.navbar .dropdown').hover(function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
    }, function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(105)
    });
});

$(document).ready(function () {
	(function( $ ) {

	    //Function to animate slider captions 
	    function doAnimations( elems ) {
			//Cache the animationend event in a variable
			var animEndEv = 'webkitAnimationEnd animationend';
			
			elems.each(function () {
				var $this = $(this),
					$animationType = $this.data('animation');
				$this.addClass($animationType).one(animEndEv, function () {
					$this.removeClass($animationType);
				});
			});
		}
		
		//Variables on page load 
		var $myCarousel = $('#carousel-example-generic'),
			$firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
			
		//Initialize carousel 
		$myCarousel.carousel();
		
		//Animate captions in first slide on page load 
		doAnimations($firstAnimatingElems);
		
		//Pause carousel  
		$myCarousel.carousel('pause');
		
		
		//Other slides to be animated on carousel slide event 
		$myCarousel.on('slide.bs.carousel', function (e) {
			var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
			doAnimations($animatingElems);
		});  
	    $('#carousel-example-generic').carousel({
	        interval:4000,
	        pause: "false"
	    });
		
	})(jQuery);	
});

$(document).ready(function(){
	var controller = new  ScrollMagic.Controller();
	var action = new ScrollMagic.Scene({
		triggerElement:'.bg-img-01',
		triggerHook: 0.8,
		reverse: false
	})
	.setClassToggle('.bg-img-01', 'fadeInRight')
	.addTo(controller);

	var action2 = new ScrollMagic.Scene({
		triggerElement:'.bg-img-02',
		triggerHook: 0.8,
		reverse: false
	})
	.setClassToggle('.bg-img-02', 'fadeInLeft')
	.addTo(controller);

	var action2 = new ScrollMagic.Scene({
		triggerElement:'#sec-01',
		triggerHook: 0.8,
		reverse: false
	})
	.setClassToggle('#sec-01', 'fadeInLeft')
	.addTo(controller);

	var action2 = new ScrollMagic.Scene({
		triggerElement:'#sec-02',
		triggerHook: 0.8,
		reverse: false
	})
	.setClassToggle('#sec-02', 'fadeInRight')
	.addTo(controller);
});

