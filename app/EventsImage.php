<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Event;

class EventsImage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'events_images';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'images', 'event_id'];

    public function Event()
    {
        return $this->belongsTo('App\Event', 'event_id');
    }

    
}
