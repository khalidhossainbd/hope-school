<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OlympiadImage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'olympiad_images';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'images', 'olympiad_id'];

    public function Olympiad()
    {
        return $this->belongsTo('App\Olympiad', 'olympiad_id');
    }
}
