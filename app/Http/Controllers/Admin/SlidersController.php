<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Slider;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Auth;

class SlidersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $sliders = Slider::where('title', 'LIKE', "%$keyword%")
                ->orWhere('images', 'LIKE', "%$keyword%")
                ->orWhere('date', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $sliders = Slider::latest()->paginate($perPage);
        }

        return view('admin.sliders.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'images' => 'required',
            'date' => 'required'
        ]);
        
        $file = Input::file('images');

        if (!empty($file)){

            $image = Image::make($file);

            $path = 'uploads/sliders/';

            $fileName = $request->file('images')->getClientOriginalName();

            //dd($fileName);
            
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);

            $image->resize(1920, 700);
            
            $image->save($path.$filenewname);

            $brimg = new Slider();
            $brimg->title = $request->title;
            $brimg->date = $request->date;
            $brimg->images = $filenewname;
            $brimg->status = $request->status;
            $brimg->save();

        }else{
            $requestData = $request->all();
        
            Slider::create($requestData);
        }

        return redirect('kadmin/sliders')->with('flash_message', 'Slider added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $slider = Slider::findOrFail($id);

        return view('admin.sliders.show', compact('slider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {

        $slider = Slider::findOrFail($id);

        return view('admin.sliders.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'date' => 'required'
        ]);
        $file = Input::file('images');

        if (!empty($file)){

            $myslider = Slider::findOrFail($id);

            $pathToImage = 'uploads/sliders/'.$myslider['images'];

            $image = Image::make($file);

            $path = 'uploads/sliders/';

            $fileName = $request->file('images')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);
            
            $image->resize(1920, 700);
            $image->save($path.$filenewname);

            if($image->save()==true){
                File::delete($pathToImage);
            }

            $myslider->title = $request->title;
            $myslider->images = $filenewname;
            $myslider->status = $request->status;
            $myslider->date = $request->date;
            $myslider->update();

        }else{
        
            $requestData = $request->all();
        
            $slider = Slider::findOrFail($id);
            $slider->update($requestData);
        }
        
        
        

        return redirect('kadmin/sliders')->with('flash_message', 'Slider updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $mypoem = Slider::find($id);

        if($mypoem['images']){
            $pathToImage = 'uploads/sliders/'.$mypoem['images'];
            File::delete($pathToImage);
        }

        $mypoem->delete();
        
        Slider::destroy($id);

        return redirect('kadmin/sliders')->with('flash_message', 'Slider deleted!');
    }
}
