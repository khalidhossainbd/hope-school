<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Campuslife;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Auth;

class CampuslifeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $campuslife = Campuslife::where('title', 'LIKE', "%$keyword%")
                ->orWhere('subtitle', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $campuslife = Campuslife::latest()->paginate($perPage);
        }

        return view('admin.campuslife.index', compact('campuslife'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.campuslife.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // dd($request);

        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'content' => 'required',
            'image' => 'required'
        ]);
        
        $file = Input::file('image');

        if (!empty($file)){

            $image = Image::make($file);

            $path = 'uploads/campus/';

            $fileName = $request->file('image')->getClientOriginalName();

            //dd($fileName);
            
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);

            $image->resize(512, 512);
            
            $image->save($path.$filenewname);

            $menu = new Campuslife();
            $menu->title = $request->title;
            $menu->subtitle = $request->subtitle;
            $menu->content = $request->content;
            $menu->image = $filenewname;
            $menu->status = $request->status;
            $menu->save();

        }else{
            $requestData = $request->all();
            
            Campuslife::create($requestData);
        }

        

        return redirect('kadmin/campuslife')->with('flash_message', 'Campuslife added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $campuslife = Campuslife::findOrFail($id);

        return view('admin.campuslife.show', compact('campuslife'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $campuslife = Campuslife::findOrFail($id);

        return view('admin.campuslife.edit', compact('campuslife'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'content' => 'required'
        ]);

        $file = Input::file('image');

        if (!empty($file)){

            $campuslife = Campuslife::findOrFail($id);
            $pathToImage = 'uploads/campus/'.$campuslife['image'];

            $image = Image::make($file);

            $path = 'uploads/campus/';

            $fileName = $request->file('image')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);
            
            $image->resize(512, 512);
            $image->save($path.$filenewname);

            if($image->save()==true){
                File::delete($pathToImage);
            }

            $campuslife->title = $request->title;
            $campuslife->subtitle = $request->subtitle;
            $campuslife->content = $request->content;
            $campuslife->image = $filenewname;
            $campuslife->status = $request->status;
            $campuslife->update();

        }else{
        
            $requestData = $request->all();
        
            $campuslife = Campuslife::findOrFail($id);
            $campuslife->update($requestData);
        }
        
        

        return redirect('kadmin/campuslife')->with('flash_message', 'Campuslife updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Campuslife::destroy($id);

        return redirect('kadmin/campuslife')->with('flash_message', 'Campuslife deleted!');
    }
}
