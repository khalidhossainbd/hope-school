<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\BranchesImage;
use Illuminate\Http\Request;

use App\Branch;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Auth;

class BranchesImagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $branchesimages = BranchesImage::where('title', 'LIKE', "%$keyword%")
                ->orWhere('images', 'LIKE', "%$keyword%")
                ->orWhere('branchactivity_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $branchesimages = BranchesImage::latest()->paginate($perPage);
        }

       
        return view('admin.branches-images.index', compact('branchesimages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $branches = Branch::where('status', '=', '1')->get();
        
        return view('admin.branches-images.create', compact('branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        

        $request->validate([
            'title' => 'required',
            'images' => 'required',
            'branchactivity_id' => 'required'
        ]);

        $file = Input::file('images');
        
        if (!empty($file))
        {
            $file_len = count($file);
            $path = 'uploads/branches/';
            //dd($file_len);
            for($i = 0; $i< $file_len; $i++)
            {
                $image = Image::make($file[$i]);

                $fileName = $file[$i]->getClientOriginalName();

                $extension = explode(".", strtolower($fileName));
                $filetype = end($extension);
                $filenewname = rand(1000000, 999999999).".".$filetype;

                $image->resize(1000, 750);
                
                $image->save($path.$filenewname);

                $brimg = new BranchesImage();
                $brimg->title = $request->title;
                $brimg->branchactivity_id = $request->branchactivity_id;
                $brimg->images = $filenewname;
                $brimg->save();

            }

        }else{
            $requestData = $request->all();
            BranchesImage::create($requestData);
        }
        

        return redirect('kadmin/branches-images')->with('flash_message', 'BranchesImage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // $branchesimage = BranchesImage::findOrFail($id);

        // return view('admin.branches-images.show', compact('branchesimage'));
        return redirect('kadmin/branches-images');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // $branchesimage = BranchesImage::findOrFail($id);

        // return view('admin.branches-images.edit', compact('branchesimage'));
        return redirect('kadmin/branches-images');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        // $requestData = $request->all();
        
        // $branchesimage = BranchesImage::findOrFail($id);
        // $branchesimage->update($requestData);

        // return redirect('kadmin/branches-images')->with('flash_message', 'BranchesImage updated!');
        return redirect('kadmin/branches-images');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $mypoem = BranchesImage::find($id);

        if($mypoem['images']){
            $pathToImage = 'uploads/branches/'.$mypoem['images'];
            File::delete($pathToImage);
        }

        $mypoem->delete();

        BranchesImage::destroy($id);

        return redirect('kadmin/branches-images')->with('flash_message', 'BranchesImage deleted!');
    }
}
