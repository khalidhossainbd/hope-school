<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Branch;
use App\Branchactivity;

class BranchactivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activity = Branchactivity::all();
        return view('admin.branches-activity.index', compact('activity'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branches = Branch::where('status', '=', 1)->get();
        return view('admin.branches-activity.create', compact('branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        
        Branchactivity::create($requestData);

        return redirect('kadmin/branches-activity')->with('flash_message', 'Branch activity added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('kadmin/branches-activity');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activity = Branchactivity::findOrFail($id);
        $branches = Branch::where('status', '=', 1)->get();
        return view('admin.branches-activity.edit', compact('activity', 'branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        
        $branch = Branchactivity::findOrFail($id);
        $branch->update($requestData);

        return redirect('kadmin/branches-activity')->with('flash_message', 'Branch activity updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect('kadmin/branches-activity');
    }

    public function ajaxactivity(Request $request)
    {
        $data = Branchactivity::select('id', 'title')->where('branch_id', $request->id)->get();

        return response()->json($data);
    }
}
