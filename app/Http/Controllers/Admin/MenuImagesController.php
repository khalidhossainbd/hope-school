<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MenuImage;
use Illuminate\Http\Request;

use App\Menu;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Auth;

class MenuImagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $menuimages = MenuImage::where('title', 'LIKE', "%$keyword%")
                ->orWhere('images', 'LIKE', "%$keyword%")
                ->orWhere('menu_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $menuimages = MenuImage::latest()->paginate($perPage);
        }

        return view('admin.menu-images.index', compact('menuimages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $menus = Menu::all();
        return view('admin.menu-images.create', compact('menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'images' => 'required',
            'menu_id' => 'required'
        ]);

        $file = Input::file('images');
        
        if (!empty($file)){


            $image = Image::make($file);

            $path = 'uploads/pages/';

            $fileName = $request->file('images')->getClientOriginalName();

            //dd($fileName);
            
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);

            $image->resize(1000, 750);
            
            
            $image->save($path.$filenewname);

            $brimg = new MenuImage();
            $brimg->title = $request->title;
            $brimg->menu_id = $request->menu_id;
            $brimg->images = $filenewname;
            $brimg->save();

        }else{
            $requestData = $request->all();
            
            MenuImage::create($requestData);
        }

        return redirect('kadmin/menu-images')->with('flash_message', 'MenuImage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $menuimage = MenuImage::findOrFail($id);

        return view('admin.menu-images.show', compact('menuimage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $menuimage = MenuImage::findOrFail($id);

        return view('admin.menu-images.edit', compact('menuimage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $menuimage = MenuImage::findOrFail($id);
        $menuimage->update($requestData);

        return redirect('kadmin/menu-images')->with('flash_message', 'MenuImage updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        MenuImage::destroy($id);

        return redirect('kadmin/menu-images')->with('flash_message', 'MenuImage deleted!');
    }
}
