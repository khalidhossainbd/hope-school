<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Sport;
use App\SportImage;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Auth;

class SportImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $eventsimage = SportImage::where('title', 'LIKE', "%$keyword%")
                ->orWhere('images', 'LIKE', "%$keyword%")
                ->orWhere('sport_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $eventsimage = SportImage::latest()->paginate($perPage);
        }

        return view('admin.sport-image.index', compact('eventsimage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sports = Sport::all();
        return view('admin.sport-image.create', compact('sports'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'images' => 'required',
            'sport_id' => 'required'
        ]);

        $file = Input::file('images');
        
        if (!empty($file))
        {
            $file_len = count($file);
            $path = 'uploads/sports/';
            //dd($file_len);
            for($i = 0; $i< $file_len; $i++)
            {
                $image = Image::make($file[$i]);

                $fileName = $file[$i]->getClientOriginalName();

                $extension = explode(".", strtolower($fileName));
                $filetype = end($extension);
                $filenewname = rand(1000000, 999999999).".".$filetype;

                $image->resize(600, 400);
                
                $image->save($path.$filenewname);

                $brimg = new SportImage();
                $brimg->title = $request->title;
                $brimg->sport_id = $request->sport_id;
                $brimg->images = $filenewname;
                $brimg->save();

            }

        }else{
            $requestData = $request->all();
            SportImage::create($requestData);
        }
        

        return redirect('kadmin/sports-images')->with('flash_message', 'Sport Image added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mypoem = SportImage::find($id);

        if($mypoem['images']){
            $pathToImage = 'uploads/sports/'.$mypoem['images'];
            File::delete($pathToImage);
        }

        $mypoem->delete();

        SportImage::destroy($id);

        return redirect('kadmin/sports-images')->with('flash_message', 'Sport Event Images deleted!');
    }
}
