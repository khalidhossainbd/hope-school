<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Menu;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Auth;


class MenusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $menus = Menu::where('mainmenu', 'LIKE', "%$keyword%")
                ->orWhere('submenu', 'LIKE', "%$keyword%")
                ->orWhere('images', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->orWhere('layout', 'LIKE', "%$keyword%")
                ->orWhere('stauts', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $menus = Menu::latest()->paginate($perPage);
        }

        return view('admin.menus.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.menus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'mainmenu' => 'required',
            'submenu' => 'required',
            'url_name' => ['required', 'string', 'regex:/^([-a-z0-9_ ])+$/i', 'unique:menuses', 'alpha_dash', 'max:255'],
            'content' => 'required',
            'status' => 'required'
        ]);
        
        $file = Input::file('images');

        if (!empty($file)){

            $image = Image::make($file);

            $path = 'uploads/pages/';

            $fileName = $request->file('images')->getClientOriginalName();

            // dd($fileName);
            
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);

            $image->resize(600, 400);
            
            $image->save($path.$filenewname);

            $menu = new Menu();
            $menu->mainmenu = $request->mainmenu;
            $menu->submenu = $request->submenu;
            $menu->url_name = $request->url_name;
            $menu->content = $request->content;
            $menu->layout = $request->layout;
            $menu->images = $filenewname;
            $menu->status = $request->status;
            $menu->save();

        }else{
            $requestData = $request->all();
            
            Menu::create($requestData);
        }
        
        

        return redirect('kadmin/menus')->with('flash_message', 'Menu added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $menu = Menu::findOrFail($id);

        return view('admin.menus.show', compact('menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $menu = Menu::findOrFail($id);

        return view('admin.menus.edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $request->validate([
            'mainmenu' => 'required',
            'submenu' => 'required',
            'url_name' => ['required', 'string', 'regex:/^([-a-z0-9_ ])+$/i', 'alpha_dash', 'max:255'],
            'content' => 'required'
        ]);

        $file = Input::file('images');

        if (!empty($file)){

            $menu = Menu::findOrFail($id);
            // dd($menu);
            $pathToImage = 'uploads/pages/'.$menu['images'];

            $image = Image::make($file);

            $path = 'uploads/pages/';

            $fileName = $request->file('images')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);
            
            $image->resize(600, 400);
            $image->save($path.$filenewname);

            if($image->save()==true){
                File::delete($pathToImage);
            }

            $menu->mainmenu = $request->mainmenu;
            $menu->submenu = $request->submenu;
            $menu->url_name = $request->url_name;
            $menu->content = $request->content;
            $menu->layout = $request->layout;
            $menu->images = $filenewname;
            $menu->status = $request->status;
            $menu->update();

        }else{
        
            $requestData = $request->all();
        
            $menu = Menu::findOrFail($id);
            $menu->update($requestData);
        }
        
        

        return redirect('kadmin/menus')->with('flash_message', 'Menu updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Menu::destroy($id);

        return redirect('kadmin/menus')->with('flash_message', 'Menu deleted!');
    }
}
