<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CampusImage;
use Illuminate\Http\Request;

use App\Campuslife;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Auth;

class CampusImagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $campusimages = CampusImage::where('title', 'LIKE', "%$keyword%")
                ->orWhere('images', 'LIKE', "%$keyword%")
                ->orWhere('campuslife_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $campusimages = CampusImage::latest()->paginate($perPage);
        }

        return view('admin.campus-images.index', compact('campusimages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $campus = Campuslife::where('status', '=', '1')->get();
        return view('admin.campus-images.create', compact('campus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'images' => 'required',
            'campuslife_id' => 'required'
        ]);

        $file = Input::file('images');

        
        if (!empty($file)){
            $file_len = count($file);
            $path = 'uploads/campus/';

            for($i = 0; $i< $file_len; $i++)
            {
                $image = Image::make($file[$i]);

                $fileName = $file[$i]->getClientOriginalName();

                //dd($fileName);
                
                $extension = explode(".", strtolower($fileName));
                $filetype = end($extension);
                $filenewname = rand(1000000, 999999999).".".$filetype;
              
                //$image->save($path.$filenewname);

                $image->resize(1000, 750);
                
                
                $image->save($path.$filenewname);

                $brimg = new CampusImage();
                $brimg->title = $request->title;
                $brimg->campuslife_id = $request->campuslife_id;
                $brimg->images = $filenewname;
                $brimg->save();
            }

        }else{
            $requestData = $request->all();
            
            CampusImage::create($requestData);
        }
        
        

        return redirect('kadmin/campus-images')->with('flash_message', 'CampusImage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $campusimage = CampusImage::findOrFail($id);

        return view('admin.campus-images.show', compact('campusimage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $campusimage = CampusImage::findOrFail($id);

        return view('admin.campus-images.edit', compact('campusimage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $campusimage = CampusImage::findOrFail($id);
        $campusimage->update($requestData);

        return redirect('kadmin/campus-images')->with('flash_message', 'CampusImage updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        CampusImage::destroy($id);

        return redirect('kadmin/campus-images')->with('flash_message', 'CampusImage deleted!');
    }
}
