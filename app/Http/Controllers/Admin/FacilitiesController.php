<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Facility;
use Illuminate\Http\Request;

use App\FacilityImage;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Auth;

class FacilitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $facilities = Facility::where('title', 'LIKE', "%$keyword%")
                ->orWhere('subtitle', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $facilities = Facility::latest()->paginate($perPage);
        }

        return view('admin.facilities.index', compact('facilities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.facilities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // dd($request);

        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'content' => 'required',
            'status' => 'required',
            'image' => 'required'
        ]);
        
        $file = Input::file('image');

        if (!empty($file)){

            $image = Image::make($file);

            $path = 'uploads/facility/';

            $fileName = $request->file('image')->getClientOriginalName();

            //dd($fileName);
            
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);

            $image->resize(400, 267);
            
            $image->save($path.$filenewname);

            $menu = new Facility();
            $menu->title = $request->title;
            $menu->subtitle = $request->subtitle;
            $menu->content = $request->content;
            $menu->image = $filenewname;
            $menu->status = $request->status;
            $menu->save();

        }else{
            $requestData = $request->all();
        
            Facility::create($requestData);
        }
       

        return redirect('kadmin/facilities')->with('flash_message', 'Facility added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $facility = Facility::findOrFail($id);
        $myimg = FacilityImage::where('facilities_id', '=', $id)->get();


        return view('admin.facilities.show', compact('facility', 'myimg'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $facility = Facility::findOrFail($id);

        return view('admin.facilities.edit', compact('facility'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'content' => 'required'
        ]);

        $file = Input::file('image');

        if (!empty($file)){

            $campuslife = Facility::findOrFail($id);
            $pathToImage = 'uploads/facility/'.$campuslife['image'];

            $image = Image::make($file);

            $path = 'uploads/facility/';

            $fileName = $request->file('image')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);
            
            $image->resize(400, 267);
            $image->save($path.$filenewname);

            if($image->save()==true){
                File::delete($pathToImage);
            }

            $campuslife->title = $request->title;
            $campuslife->subtitle = $request->subtitle;
            $campuslife->content = $request->content;
            $campuslife->image = $filenewname;
            $campuslife->status = $request->status;
            $campuslife->update();

        }else{
        
            $requestData = $request->all();
            
            $facility = Facility::findOrFail($id);
            $facility->update($requestData);
        }
        

        return redirect('kadmin/facilities')->with('flash_message', 'Facility updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Facility::destroy($id);

        return redirect('kadmin/facilities')->with('flash_message', 'Facility deleted!');
    }
}
