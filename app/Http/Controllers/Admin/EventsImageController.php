<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\EventsImage;
use Illuminate\Http\Request;

use App\Event;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Auth;

class EventsImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $eventsimage = EventsImage::where('title', 'LIKE', "%$keyword%")
                ->orWhere('images', 'LIKE', "%$keyword%")
                ->orWhere('events_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $eventsimage = EventsImage::latest()->paginate($perPage);
        }

        return view('admin.events-image.index', compact('eventsimage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $events = Event::where('status', '=', '1')->get();
        return view('admin.events-image.create', compact('events'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'images' => 'required',
            'event_id' => 'required'
        ]);

        $file = Input::file('images');
        
        if (!empty($file))
        {
            $file_len = count($file);
            $path = 'uploads/events/';
            //dd($file_len);
            for($i = 0; $i< $file_len; $i++)
            {
                $image = Image::make($file[$i]);

                $fileName = $file[$i]->getClientOriginalName();

                $extension = explode(".", strtolower($fileName));
                $filetype = end($extension);
                $filenewname = rand(1000000, 999999999).".".$filetype;

                $image->resize(600, 400);
                
                $image->save($path.$filenewname);

                $brimg = new EventsImage();
                $brimg->title = $request->title;
                $brimg->event_id = $request->event_id;
                $brimg->images = $filenewname;
                $brimg->save();
            }

        }else{
            $requestData = $request->all();
            
            EventsImage::create($requestData);
        }
        
        return redirect('kadmin/events-image')->with('flash_message', 'EventsImage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $eventsimage = EventsImage::findOrFail($id);

        return view('admin.events-image.show', compact('eventsimage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $eventsimage = EventsImage::findOrFail($id);

        return view('admin.events-image.edit', compact('eventsimage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $eventsimage = EventsImage::findOrFail($id);
        $eventsimage->update($requestData);

        return redirect('kadmin/gallery-images')->with('flash_message', 'EventsImage updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $mypoem = EventsImage::find($id);

        if($mypoem['images']){
            $pathToImage = 'uploads/events/'.$mypoem['images'];
            File::delete($pathToImage);
        }

        $mypoem->delete();

        EventsImage::destroy($id);

        return redirect('kadmin/events-image')->with('flash_message', 'EventsImage deleted!');
    }
}
