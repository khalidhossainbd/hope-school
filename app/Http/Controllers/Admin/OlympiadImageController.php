<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\OlympiadImage;
use App\Olympiad;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Auth;

class OlympiadImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $eventsimage = OlympiadImage::where('title', 'LIKE', "%$keyword%")
                ->orWhere('images', 'LIKE', "%$keyword%")
                ->orWhere('olympiad_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $eventsimage = OlympiadImage::latest()->paginate($perPage);
        }

        return view('admin.olympiad-images.index', compact('eventsimage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $events = Olympiad::all();
        return view('admin.olympiad-images.create', compact('events'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'title' => 'required',
            'images' => 'required',
            'olympiad_id' => 'required'
        ]);

        $file = Input::file('images');
        
        if (!empty($file))
        {
            $file_len = count($file);
            $path = 'uploads/Olympiad/';
            //dd($file_len);
            for($i = 0; $i< $file_len; $i++)
            {
                $image = Image::make($file[$i]);

                $fileName = $file[$i]->getClientOriginalName();

                $extension = explode(".", strtolower($fileName));
                $filetype = end($extension);
                $filenewname = rand(1000000, 999999999).".".$filetype;

                $image->resize(600, 400);
                
                $image->save($path.$filenewname);

                $brimg = new OlympiadImage();
                $brimg->title = $request->title;
                $brimg->olympiad_id = $request->olympiad_id;
                $brimg->images = $filenewname;
                $brimg->save();

            }

        }else{
            $requestData = $request->all();
            OlympiadImage::create($requestData);
        }
        

        return redirect('kadmin/olympiad-images')->with('flash_message', 'Olympiad Image added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mypoem = OlympiadImage::find($id);

        if($mypoem['images']){
            $pathToImage = 'uploads/Olympiad/'.$mypoem['images'];
            File::delete($pathToImage);
        }

        $mypoem->delete();

        OlympiadImage::destroy($id);

        return redirect('kadmin/olympiad-images')->with('flash_message', 'Event Images deleted!');
    }
}
