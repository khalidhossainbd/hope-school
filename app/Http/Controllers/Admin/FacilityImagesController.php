<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\FacilityImage;
use Illuminate\Http\Request;

use App\Facility;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Auth;

class FacilityImagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $facilityimages = FacilityImage::where('title', 'LIKE', "%$keyword%")
                ->orWhere('images', 'LIKE', "%$keyword%")
                ->orWhere('facilities_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $facilityimages = FacilityImage::latest()->paginate($perPage);
        }

        return view('admin.facility-images.index', compact('facilityimages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $myfacility = Facility::where('status', '=', '1')->get();
        return view('admin.facility-images.create', compact('myfacility'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {


        $request->validate([
            'title' => 'required',
            'images' => 'required',
            'facilities_id' => 'required'
        ]);

        $file = Input::file('images');
        
        if (!empty($file))
        {
            $file_len = count($file);
            $path = 'uploads/facility/';
            //dd($file_len);
            for($i = 0; $i< $file_len; $i++){
                $image = Image::make($file[$i]);

                $fileName = $file[$i]->getClientOriginalName();

                $extension = explode(".", strtolower($fileName));
                $filetype = end($extension);
                $filenewname = rand(1000000, 999999999).".".$filetype;

                $image->resize(1000, 750);
                
                $image->save($path.$filenewname);

                $brimg = new FacilityImage();
                $brimg->title = $request->title;
                $brimg->facilities_id = $request->facilities_id;
                $brimg->images = $filenewname;
                $brimg->save();
            }

        }else{
            $requestData = $request->all();
            FacilityImage::create($requestData);
        }
        
        
        return redirect('kadmin/facility-images')->with('flash_message', 'FacilityImage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $facilityimage = FacilityImage::findOrFail($id);

        return view('admin.facility-images.show', compact('facilityimage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $facilityimage = FacilityImage::findOrFail($id);

        return view('admin.facility-images.edit', compact('facilityimage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $facilityimage = FacilityImage::findOrFail($id);
        $facilityimage->update($requestData);

        return redirect('kadmin/facility-images')->with('flash_message', 'FacilityImage updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        FacilityImage::destroy($id);

        return redirect('kadmin/facility-images')->with('flash_message', 'FacilityImage deleted!');
    }
}
