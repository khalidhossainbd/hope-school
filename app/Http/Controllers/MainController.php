<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Menu;
use App\Branch;
use App\Campuslife;
use App\Facility;
use App\FacilityImage;
use App\Event;
use App\Slider;
use App\PhotoGallery;
use App\VideoGallery;

use App\Olympiad;
use App\Sport;

class MainController extends Controller
{
    public function index()
    {  
        $campus = Campuslife::where('status', '=', '1')->get();
        $myfacility = Facility::where('status', '=', '1')->get();
        $myevents = Event::where('status', '=', '1')->orderBy('created_at', 'desc')->limit(9)->get();
        $myslider = Slider::where('status', '=', '1')->orderBy('date', 'desc')->get();

        return view('pages.new', compact('campus', 'myfacility', 'myevents', 'myslider'));
    }



    public function pages($url)
    {
        
        $newmenus = Menu::where('url_name', '=', $url)->first();

        if($newmenus->layout == 1){
             return view('pages.menus.showlayoutone', compact('newmenus'));
        }elseif($newmenus->layout == 2){
            return view('pages.menus.showlayouttwo', compact('newmenus'));
        }elseif($newmenus->layout == 3){
            return view('pages.menus.showlayoutthree', compact('newmenus'));
        }else{
            return view('pages.menus.showlayoutfour', compact('newmenus'));
        }

       
    }

    public function branches($url)
    {  
        $mybranch = Branch::where('url_name', '=', $url)->first();;

        return view('pages.menus.branchpage', compact('mybranch'));
    }

    public function campuslife($id)
    {
        
        $mycampus = Campuslife::findOrFail($id);

        return view('pages.menus.mycampuslife', compact('menuone', 'menutwo', 'menuthree', 'menufour', 'mycampus'));

    }

    public function schoolfacility($id)
    {       
        $myfacility = Facility::findOrFail($id);
        $myfacilityimg = FacilityImage::where('facilities_id', '=', $id)->get();
        return view('pages.facilities.facilitiesdetails', compact('myfacility', 'myfacilityimg'));
    }

    public function schoolevents($id)
    {
        $myevent = Event::findOrFail($id);
        return view('pages.events.eventdetails', compact('myevent'));
    }

    

    public function history()
    {
       	return view('pages.aboutus.history');
    }

	public function message()
    {
    	return view('pages.aboutus.message');
    }

	public function speech()
    {
    	return view('pages.aboutus.speech');
    }

	public function whywe()
    {
    	return view('pages.aboutus.whywe');
    }

	public function mission()
    {
    	return view('pages.aboutus.mission');
    }

	public function olavel()
    {
    	return view('pages.achievements.olavel');
    }
    public function sports()
    {   
        $sports = Sport::where('status', '=', '1')->orderBy('sl_date', 'ASC')->get();
        return view('pages.achievements.sports', compact('sports'));
    }

    public function olympiads()
    {
        $olympiad = Olympiad::where('status', '=', '1')->orderBy('sl_date', 'ASC')->get();

        return view('pages.achievements.olympiads', compact('olympiad'));
    }

    public function enrollment()
    {
        return view('pages.achievements.enrollment');
    }

    public function curriculum()
    {
        return view('pages.academics.curriculum');
    }
    public function academic_calendar()
    {
        return view('pages.academics.academicCalendar');
    }
    public function scholarship()
    {
        return view('pages.academics.scholarship');
    }
    public function clubs()
    {
        return view('pages.academics.clubs');
    }
    public function career_opportunity()
    {
        return view('pages.academics.careeropportunity');
    }
    public function extra_curricular_activities()
    {
        return view('pages.academics.extra-curricular-activities');
    }


    public function photogallery()
    {  
        $photos = PhotoGallery::withCount('GalleryImage')->where('status', '=', '1')->orderBy('id', 'ASC')->paginate(5);

        return view('pages.gallery.photogallery', compact('photos'));
    }

    public function videogallery()
    {
        $myvideo = VideoGallery::where('status', '=', '1')->orderBy('date','DESC')->get();

        return view('pages.gallery.videogallery', compact('myvideo'));
    }

    public function contuct_us()
    {
        return view('pages.contuct');
    }
}
