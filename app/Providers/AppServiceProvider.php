<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

use App\Branch;
use App\Menu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $menuone = Menu::where('status', '=', '1')->where('mainmenu', '=', '1')->get();
        $menutwo = Menu::where('status', '=', '1')->where('mainmenu', '=', '2')->get();
        $menuthree = Menu::where('status', '=', '1')->where('mainmenu', '=', '3')->get();
        $menufour = Menu::where('status', '=', '1')->where('mainmenu', '=', '4')->get();
        $branches = Branch::where('status', '=', '1')->get();
        View::share('branches', $branches);
        View::share('menuone', $menuone);
        View::share('menutwo', $menutwo);
        View::share('menuthree', $menuthree);
        View::share('menufour', $menufour);
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
