<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Menu;

class MenuImage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu_images';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'images', 'menu_id'];

    public function Menu()
    {
        return $this->belongsTo('App\Menu', 'menu_id');
    }
    
}
