<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sport extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sports';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'content', 'status', 'sl_date'];

    public function SportImage()
    {
        return $this->hasMany('App\SportImage');
    }
}
