<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branchactivity extends Model
{
    protected $table = 'branchactivities';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'branch_id', 'content', 'status', 'sl_date' ];

    public function Branch()
    {
        return $this->belongsTo('App\Branch', 'branch_id');
    }

    public function BranchesImage()
    {
        return $this->hasMany(BranchesImage::class);
    }
    
}
