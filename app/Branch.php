<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\BranchesImage;

class Branch extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'branches';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'url_name', 'address', 'content', 'status', 'facebook', 'youtube', 'twitter'];

    public function Branchactivity()
    {
        return $this->hasMany('App\Branchactivity')->where('status','=', 1);
    }
    
    public function BranchesImage()
    {
        return $this->hasManyThrough('App\BranchesImage', 'App\Branchactivity', 'branch_id', 'branchactivity_id', 'id', 'id');
    }


    public function getRouteKeyName()
    {
        return 'url_name';
    }
}
