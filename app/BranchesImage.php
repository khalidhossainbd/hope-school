<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchesImage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'branches_images';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'images', 'branchactivity_id'];

    public function Branchactivity()
    {
        return $this->belongsTo(Branchactivity::class, 'branchactivity_id');
    }
    
    public function Branch()
    {
        return $this->hasManyThrough('App\Branch', 'App\Branchactivity', 'branchactivity_id', 'branch_id', 'id', 'id');
    }
}
