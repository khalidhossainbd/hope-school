<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\EventsImage;

class Event extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'subtitle', 'date', 'image', 'status', 'content'];

    public function EventsImage()
    {
        return $this->hasMany('App\EventsImage');
    }

    
}
