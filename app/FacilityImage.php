<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Facility;

class FacilityImage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'facility_images';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'images', 'facilities_id'];


    public function Facility()
    {
        return $this->belongsTo('App\Facility', 'facilities_id');
    }
}
