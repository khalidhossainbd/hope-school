<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\CampusImage;


class Campuslife extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'campuslives';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'subtitle', 'image', 'content', 'status'];

    public function CampusImage()
    {
        return $this->hasMany('App\CampusImage');
    }
    
}
