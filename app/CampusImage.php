<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Campuslife;

class CampusImage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'campus_images';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'images', 'campuslife_id'];


    public function Campuslife()
    {
        return $this->belongsTo('App\Campuslife', 'campuslife_id');
    }

    
}
