<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SportImage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sport_images';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'images', 'sport_id'];

    public function Sport()
    {
        return $this->belongsTo('App\Sport', 'sport_id');
    }
}
