<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Olympiad extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'olympiads';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'content', 'status', 'sl_date'];

    public function OlympiadImage()
    {
        return $this->hasMany('App\OlympiadImage');
    }
}
