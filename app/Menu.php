<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\MenuImage;

class Menu extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menuses';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['mainmenu', 'submenu', 'url_name', 'images', 'content', 'layout', 'status'];

    public function MenuImage()
    {
        return $this->hasMany('App\MenuImage');
    }


    public function getRouteKeyName()
    {
        return 'url_name';
    }
    
}
