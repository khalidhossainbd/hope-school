<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This application design and develop by Md Khalid Hossain
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('config/cache',function(){
$exitCode = Artisan::call('config:cache');

}); 


Route::get('logout',function(){
    return view('errors.404');
});

Route::get('/', 'MainController@index')->name('home');
// Route::get('/indexnew', 'MainController@indexnew');

Route::get('/brief_history', 'MainController@history')->name('history');
Route::get('/chairman_message', 'MainController@message')->name('chairman_message');
Route::get('/inaugration_speech', 'MainController@speech')->name('inaugration_speech');
Route::get('/why_ihsb', 'MainController@whywe')->name('why_ihsb');
Route::get('/mission_vission', 'MainController@mission')->name('mission_vission');

// Achivements Link
Route::get('/o_&_a_lavel', 'MainController@olavel');
Route::get('/sports', 'MainController@sports');
Route::get('/olympiads', 'MainController@olympiads');
Route::get('/university_enrollment', 'MainController@enrollment');
Route::get('/photo_gallery', 'MainController@photogallery');
Route::get('/video_gallery', 'MainController@videogallery');
Route::get('/contuct_us', 'MainController@contuct_us');

// Academics Pages Links

Route::get('/curriculum', 'MainController@curriculum');
Route::get('/academic_calendar', 'MainController@academic_calendar');
Route::get('/scholarship', 'MainController@scholarship');
Route::get('/clubs', 'MainController@clubs');
Route::get('/career_opportunity', 'MainController@career_opportunity');
Route::get('/extra_curricular_activities', 'MainController@extra_curricular_activities');



Route::get('/{url_name}', 'MainController@pages');

Route::get('/branches/{url_name}', 'MainController@branches');

Route::get('/campus-life/{id}', 'MainController@campuslife');
Route::get('/school-facility/{id}', 'MainController@schoolfacility');
Route::get('/school-events/{id}', 'MainController@schoolevents');

Route::group([ 'prefix' => 'kadmin' ], function() {
	Auth::routes();
	Route::get('/home', 'HomeController@index')->name('dashboard');

});

Route::group(["namespace"=>"Admin"],function() {
    Route::get('/kadmin/find_activity', 'BranchactivityController@ajaxactivity');
});



Route::resource('kadmin/branches', 'Admin\\BranchesController');
Route::resource('kadmin/branches-activity', 'Admin\\BranchactivityController');
Route::resource('kadmin/branches-images', 'Admin\\BranchesImagesController');
Route::resource('kadmin/sliders', 'Admin\\SlidersController');
Route::resource('kadmin/campuslife', 'Admin\\CampuslifeController');
Route::resource('kadmin/campus-images', 'Admin\\CampusImagesController');
Route::resource('kadmin/facilities', 'Admin\\FacilitiesController');
Route::resource('kadmin/facility-images', 'Admin\\FacilityImagesController');
Route::resource('kadmin/menus', 'Admin\\MenusController');
Route::resource('kadmin/menu-images', 'Admin\\MenuImagesController');
Route::resource('kadmin/events', 'Admin\\EventsController');
Route::resource('kadmin/events-image', 'Admin\\EventsImageController');
Route::resource('kadmin/photo-gallery', 'Admin\\PhotoGalleryController');
Route::resource('kadmin/gallery-images', 'Admin\\GalleryImageController');
Route::resource('kadmin/video-gallery', 'Admin\\VideoGalleryController');
Route::resource('kadmin/sports', 'Admin\\SportsController');
Route::resource('kadmin/sports-images', 'Admin\\SportImageController');
Route::resource('kadmin/bangla-olympiad', 'Admin\\OlympiadController');
Route::resource('kadmin/olympiad-images', 'Admin\\OlympiadImageController');


Route::get('/{any}', function ($any) {

  return Redirect::to('/');

})->where('any', '.*');
